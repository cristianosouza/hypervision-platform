﻿using Commons_Core.Globals;
using Commons_Core.Services.LogService;
using hypervision_platform.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace hypervision_platform.Services
{
    public class ConfigurationService : IConfigurationService
    {
        private static readonly string _path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "config");

        public List<string> GetFields()
        {
            string fieldsConfig = Globals.Configuration.GetSection("Fields")?.Value;

            if (!string.IsNullOrEmpty(fieldsConfig))
            {
                return new List<string>(fieldsConfig.Split(','));
            }

            LogServiceConnection.Warning<string>($"Nenhuma área de atuação configurada");

            return new List<string>();

        }

        public List<string> GetUrls()
        {
            string urls = Environment.GetEnvironmentVariable("URLS");

            if (!string.IsNullOrEmpty(urls))
            {
                return new List<string>(urls.Split(','));
            }

            LogServiceConnection.Warning<string>($"Nenhuma área de atuação configurada");

            return new List<string>();
        }

        public string UploadSoundFile(IFormFile file)
        {
            try
            {
                //TODO:  validar se arquivo é realmente áudio

                if (file.Length > 0)
                {
                    var filePath = Path.Combine(_path, "audio");
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    var fileAudioPath = Path.Combine(filePath, $"{file.FileName}");
                    var tempFilePath = Path.GetTempFileName();

                    using (var stream = File.Create(tempFilePath))
                    {
                        file.CopyTo(stream);
                    }

                    File.Copy(tempFilePath, fileAudioPath);

                    LogServiceConnection.Debug<string>($"Arquivo de som salvo: {fileAudioPath}");
                    return fileAudioPath;
                }

                LogServiceConnection.Error<string>($"Arquivo de som inválido.");
                return null;
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao salvar aquivo de som: {ex.Message}");
                return null;
            }
        }
    }
}
