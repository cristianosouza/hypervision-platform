﻿using Commons_Core.Services.LogService;
using hypervision_platform.DAO;
using hypervision_platform.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace hypervision_platform.Services
{
    public class MergeCard
    {
        private readonly RdoDAO _rdoDAO;
        public MergeCard()
        {
            _rdoDAO = new RdoDAO(new MongoContext());
        }

        public Card MergeRDOs(string user, Card mainCard, List<Card> cards, string comment)
        {
            List<string> mergedCardsIds = new List<string>();

            var mainRDO = _rdoDAO.GetRDOByIdOrThrowsException(mainCard.CardId);
            var cardEvent = GetMergeCardEvent(user, comment);

            foreach (var card in cards)
            {
                ValidateIfCardsCanBeMerged(mainCard, card);

                List<Card> rdo = _rdoDAO.GetRDOById(card.CardId);

                if (rdo == null || rdo.Count == 0)
                {
                    LogServiceConnection.Error<string>($"Cartão {card.CardId} não encontrado.");
                    continue;
                }
                if (!string.IsNullOrEmpty(rdo.Last().MergedCardId))
                {
                    LogServiceConnection.Error<string>($"Cartão {card.CardId} já foi mesclado.");
                    continue;
                }

                mainCard = MergeCards(mainCard, card);
                mainRDO.AddRange(rdo);
                mergedCardsIds.Add(card.CardId);

                card.Event = cardEvent;
                card.MergedCardId = mainCard.CardId;

                _rdoDAO.UpdateCard(card);
            }

            mainCard.Event = cardEvent;
            mainCard.Event.MergedCardsIds = mergedCardsIds;
            mainCard.Merged = true;

            _rdoDAO.UpdateCard(mainCard);
            LogServiceConnection.Debug<string>($"Cartão {mainCard.CardId} mesclado.");

            return mainCard;
        }

        private void ValidateIfCardsCanBeMerged(Card mainCard, Card card)
        {
            if (!mainCard.EventType.Equals(card.EventType))
            {
                string errorMessage = "Cartões de tipos diferentes não podem ser mesclados.";
                LogServiceConnection.Error<string>(errorMessage);
                throw new Exception(errorMessage);
            }
            // TODO: CDF-696: alertas de manobra não podem ser mesclados
            /* if (!mainCard.EventType.Equals(EventType.Maneuver))
            {
                string errorMessage = "Cartões de manobra não podem ser mesclados.";
                LogServiceConnection.Error<string>(errorMessage);
                throw new Exception(errorMessage);
            }*/
        }

        private Card MergeCards(Card card1, Card card2)
        {
            card1.Dispached = (card1.Dispached || card2.Dispached);
            card1.Date = card1.Date <= card2.Date ? card1.Date : card2.Date;
            card1.EventStart = card1.EventStart <= card2.EventStart ? card1.EventStart : card2.EventStart;
            card1.EventEnd = card1.EventEnd >= card2.EventEnd ? card1.EventEnd : card2.EventEnd;
            card1.Severity = card1.Severity >= card2.Severity ? card1.Severity : card2.Severity;
            card1.CardDetails = MergeCardDetails(card1.CardDetails, card2.CardDetails);

            return card1;
        }

        private CardDetails MergeCardDetails(CardDetails card1, CardDetails card2)
        {
            card1.Cities = card1.Cities?.Union(card2.Cities)?.ToList();
            card1.Lines = card1.Lines?.Union(card2.Lines)?.ToList();
            card1.Regions = card1.Regions?.Union(card2.Regions)?.ToList();
            card1.Structures = card1.Structures?.Union(card2.Structures)?.ToList();
            card1.SubRegions = card1.SubRegions?.Union(card2.SubRegions)?.ToList();
            card1.Substations = card1.Substations?.Union(card2.Substations)?.ToList();

            // TODO: merge properties
            return card1;
        }

        private CardEvent GetMergeCardEvent(string user, string comment)
        {
            return new CardEvent
            {
                Date = DateTime.Now.ToUniversalTime(),
                Description = "Cartões mesclados",
                Type = ActionType.Merge,
                User = user,
                Comment = comment
            };
        }

    }
}
