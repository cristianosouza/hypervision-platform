﻿using Commons_Core.Services.LogService;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;

namespace hypervision_platform.Services
{
    // TODO: mover para o commons core após atualizar versão
    public class AuthenticationControl
    {
        private static HttpClient client = new HttpClient();
        private static string ProductName = "Hypervision";
        private static string SaveModulesAndFeaturesForProductAPIUrl = $"Product/SaveProductModulesAndFeatures/{ProductName}";

        public AuthenticationControl()
        {
            string authenticationServer = Environment.GetEnvironmentVariable("AUTHENTICATION_SERVER");
            client.BaseAddress = new Uri(authenticationServer);
        }

        public async void RegisterModulesAndFeaturesOnAuthenticationService()
        {
            object modulesConfiguration = ReadModulesConfigurationFile();
            if (modulesConfiguration == null)
            {
                return;
            }
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync(SaveModulesAndFeaturesForProductAPIUrl, modulesConfiguration);
                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao enviar módulos e features ao serviço de autenticação: {ex.Message}");
            }
        }

        private object ReadModulesConfigurationFile()
        {
            try
            {
                string modulesConfigurationPath = Path.Combine(Directory.GetCurrentDirectory(), "modules.json");
                string modules = File.ReadAllText(modulesConfigurationPath);

                return JsonConvert.DeserializeObject(modules);
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao ler configuração de módulos e features: {ex.Message}");
                return null;
            }
        }

    }
}
