﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace hypervision_platform.Services.Interfaces
{
    public interface IConfigurationService
    {
        List<string> GetFields();
        List<string> GetUrls();
        string UploadSoundFile(IFormFile file);
    }
}
