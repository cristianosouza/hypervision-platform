import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConcertTableIoModule } from '@xst/concert-table-io';
import { EventManagementComponent } from './event-management.component';
import { EventManagementRoutingModule } from './event-management.routing.module';

@NgModule({
  declarations: [
    EventManagementComponent
  ],
  imports: [
    CommonModule,
    ConcertTableIoModule,
    FormsModule,
    ReactiveFormsModule,
    EventManagementRoutingModule
  ],
  providers: [ ]
})
export class EventManagementModule { }
