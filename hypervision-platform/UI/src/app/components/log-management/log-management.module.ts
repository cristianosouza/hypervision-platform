import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LogManagementComponent } from './log-management.component';
import { LogManagementRoutingModule } from './log-management.routing.module';

@NgModule({
  declarations: [
    LogManagementComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LogManagementRoutingModule
  ],
  providers: [ ]
})
export class LogManagementModule { }
