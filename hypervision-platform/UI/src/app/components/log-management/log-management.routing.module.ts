import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LogManagementComponent } from './log-management.component';

const routes: Routes = [{ path: '', component: LogManagementComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LogManagementRoutingModule { }