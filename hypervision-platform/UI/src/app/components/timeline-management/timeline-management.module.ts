import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TimelineManagementComponent } from './timeline-management.component';
import { TimelineManagementRoutingModule } from './timeline-management.routing.module';

@NgModule({
  declarations: [
    TimelineManagementComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TimelineManagementRoutingModule
  ],
  providers: [ ]
})
export class TimelineManagementModule { }
