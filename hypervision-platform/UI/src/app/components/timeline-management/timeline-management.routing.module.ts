import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TimelineManagementComponent } from './timeline-management.component';

const routes: Routes = [{ path: '', component: TimelineManagementComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TimelineManagementRoutingModule { }