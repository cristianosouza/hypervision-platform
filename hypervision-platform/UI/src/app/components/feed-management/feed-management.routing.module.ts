import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FeedManagementComponent } from './feed-management.component';

const routes: Routes = [{ path: '', component: FeedManagementComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeedManagementRoutingModule { }