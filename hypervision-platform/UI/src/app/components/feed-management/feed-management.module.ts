import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FeedManagementComponent } from './feed-management.component';
import { FeedManagementRoutingModule } from './feed-management.routing.module';

@NgModule({
  declarations: [
    FeedManagementComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FeedManagementRoutingModule
  ],
  providers: [ ]
})
export class FeedManagementModule { }
