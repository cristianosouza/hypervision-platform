import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConcertTableIoModule } from '@xst/concert-table-io';
import { NavbarManagementComponent } from './navbar-management.component';
import { NavbarManagementRoutingModule } from './navbar-management.routing.module';

@NgModule({
  declarations: [
    NavbarManagementComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ConcertTableIoModule,
    NavbarManagementRoutingModule
  ],
  providers: [ ]
})
export class NavbarManagementModule { }
