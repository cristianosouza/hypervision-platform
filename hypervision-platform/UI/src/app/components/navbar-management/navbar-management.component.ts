import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/services/common.service';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { ConcertTable } from '@xst/concert-table-io';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-navbar-management',
  templateUrl: './navbar-management.component.html',
  styleUrls: ['./navbar-management.component.scss']
})
export class NavbarManagementComponent implements OnInit {

  editAppData: any;
  editApp = false;
  modalRef: any;
  modalMessage: string;
  listApplications: any[] = [];
  navbarForm: FormGroup;
  applicationForm: FormGroup;
  addAplication = false;
  imageUrl: any;
  tableConfig: ConcertTable = {
    registrationAmount: 0,
    hideColumnSelectAll: true,
    column: [],
    attributeName: [],
    data: [],
    actionList: [],
    actionListTop: [],
  };
  removeApp = false;
  appToRemove: any;
  editData: any;
  configuration: any;
  navbar: any;
  filePath: string;

  logLevels: any[] = [
    { value: "_blank" },
    { value: "_self" },
  ];

  @ViewChild('modalConfirm') private modalConfirm: TemplateRef<any>;

  constructor(
    private modalService: NgbModal,
    private fb: FormBuilder,
    private services: CommonService
  ) { }

  ngOnInit(): void {
    this.getConfig();
  }

  // get f() { return this.navbarForm.controls; }

  openModal(): void {
    this.modalRef = this.modalService.open(this.modalConfirm);
  }

  closeEdit(): void {
    this.editApp = false;
    this.addAplication = false;
    // this.submitted = false;
  }

  initializeTable() {
    this.tableConfig.data = this.listApplications;
    this.tableConfig.column = this.getColumns();
    this.tableConfig.attributeName = this.getAttributeColumnsNames();
    this.tableConfig.actionList = this.getActionList();
    this.tableConfig.actionListTop = this.getActionListTop();
    this.tableConfig.registrationAmount = this.listApplications.length ? this.listApplications.length : 0;

    this.updateTableData();
  }


  getColumns() {
    return [
      { name: 'Nome' },
      { name: 'Url' },
      { name: 'Ações', width: '200px' }
    ];
  }

  getAttributeColumnsNames() {
    return [
      { name: 'nameApp' },
      { name: 'urlApp' },
    ];
  }

  getActionList() {
    return [
      {
        name: "Editar",
        class: "btn btn-outline-primary btn-sm mr-2",
        icon: "fa fa-edit",
        callback: (data) => {
          this.modalMessage = "Salvar alterações?";
          this.editApp = true;
          this.editAppData = data;
          this.initializeApplicationForm();
        }
      },
      {
        name: "Remover",
        class: "btn btn-outline-laranja-4 btn-sm mr-2",
        icon: "fa fa-trash",
        callback: (data) => {
          this.appToRemove = data;
          this.modalMessage = "Excluir aplicativo selecionado?";
          this.removeApp = true;
          this.openModal();

          this.updateTableData();
        }
      }
    ]
  }

  getActionListTop() {
    return [
      {
        name: "Novo",
        class: "btn btn-info add-user-button btn-sm mr-2",
        icon: "fa fa-plus",
        callback: (data) => {
          this.editAppData = { nameApp: '', urlApp: '', icon: '', titleApp: '', target: '' };
          this.initializeApplicationForm();
          this.addAplication = true;
          this.modalMessage = "Adicionar Aplicativo?";
        }
      }
    ];
  }

  getConfig() {
    this.services.get(environment.endPoints.getConfiguration).subscribe(d => {

      this.configuration = d;
      if (!this.configuration.navbarSettings)
        this.initializeConfig();
      else
        this.navbar = this.configuration.navbarSettings;

      this.initializeForm();
    }, err => {
      console.error(err);
    });
  }

  initializeConfig() {
    let defaultColor = "#000000";
    this.navbar = {
      color: defaultColor,
      showNotification: true,
      mainLogo: "",
      mainName: "",
      fontSize: 12,
      photo: "",
      nameFilters: "",
      multipleFilters: true,
      applications: []
    };
  }

  initializeForm() {
    this.navbarForm = this.fb.group({
      colorNavbar: [this.navbar.color],
      showNotification: [this.navbar.showNotification],
      mainLogo: [this.navbar.mainLogo],
      mainName: [this.navbar.mainName],
      fontSize: [this.navbar.fontSize],
      photo: [this.navbar.photo],
      nameFilters: [this.navbar.nameFilters],
      multipleFilters: [this.navbar.multipleFilters],
    });

    this.listApplications = this.navbar.applications;

    this.initializeTable();
  }

  initializeApplicationForm() {
    this.applicationForm = this.fb.group({
      id: [this.editApp ? this.editAppData.id : new Date().getTime()],
      nameApp: [this.editAppData.nameApp],
      urlApp: [this.editAppData.urlApp],
      icon: [this.editAppData.icon],
      titleApp: [this.editAppData.titleApp],
      target: [this.editAppData.target]
    });
  }

  confirm() {
    if (this.editApp || this.addAplication)
      this.pushApplication();
    else if (this.removeApp)
      this.removeApplication();
    else
      this.saveConfirm();
  }

  updateTableData() {
    this.tableConfig = Object.assign({}, this.tableConfig);
  }

  receiveFeedback(data) {
    console.log(data);
  }

  loadImage(event, id) {
    let reader = new FileReader();
    reader.onload = (e) => {
      this.imageUrl = e.target.result;
    }

    var selectedFile = <FileList>event.srcElement.files;
    reader.readAsDataURL(selectedFile[0]);
    document.getElementById(id).innerHTML = selectedFile[0].name;
  }

  pushApplication() {
    let application = {
      id: this.applicationForm.value.id,
      nameApplication: this.applicationForm.value.nameApp,
      url: this.applicationForm.value.urlApp,
      icon: this.applicationForm.value.icon,
      hint: this.applicationForm.value.titleApp,
      target: this.applicationForm.value.target
    };

    // let index = this.configuration.applications.findIndex(m => m.id == application.id);
    // let index = this.tableConfig.data.findIndex(m => m.nameApp == application.nameApp);
    let index = this.listApplications.findIndex(m => m.id == application.id);
    if (this.editApp) {
      // this.configuration.applications[index] = application;
      this.listApplications[index] = application;
    } else {
      // this.configuration.applications.push(application);
      this.listApplications.push(application)
    }

    this.modalRef.close();

    this.addAplication = false;
    this.editApp = false;

    this.updateTableData();
  }

  removeApplication() {
    // let index = this.configuration.applications.findIndex(m => m.id === this.appToRemove.id);
    // this.configuration.applications.splice(index, 1);
    let index = this.listApplications.findIndex(m => m.id == this.appToRemove.id);
    this.listApplications.splice(index, 1);

    index = this.tableConfig.data.findIndex(m => m.nameApp === this.appToRemove.nameApp);
    this.tableConfig.data.splice(index, 1);

    this.modalRef.close();
    this.removeApp = false;

    this.updateTableData();
  }

  saveConfirm() {
    // this.submitted = true;
    // if (this.eventForm.invalid) {
    //   this.modalRef.close();
    //   return;
    // }
    var objData = {
      backgroundColor: this.navbarForm.value.color,
      showNotification: this.navbarForm.value.showNotification,
      mainLogo: {
        image: this.navbarForm.value.mainLogo,
        name: this.navbarForm.value.mainName,
        fontSize: this.navbarForm.value.fontSize
      },
      filters: {
        name: this.navbarForm.value.nameFilters,
        multiple: this.navbarForm.value.multipleFilters,
      }
      // applications: this.listApplications
    }

    this.configuration.navbarConfigurations = objData;

    this.modalRef.close();
    let url = `${environment.endPoints.saveConfiguration}${this.services.user.user}`;
    this.services.post(url, this.configuration).subscribe(
      //try
      response => {
        console.log('Operação realizada com sucesso!')

        // this.tableConfig.data = this.listEvents;
        this.updateTableData();
      },
      //catch
      error => { console.log('Erro ao salvar!') })
      //finally
      .add(() => {
        this.closeEdit();
      }
    );
  }

  onChange(event){
    var selectedFile = <FileList>event.srcElement.files;
    document.getElementById('fileLabel').innerHTML = selectedFile[0].name;

    var file = selectedFile[0];
    var url = `${environment.endPoints.uploadFile}`;

    this.services.uploadFile(url, file).subscribe(
      //try
      response => {
        this.filePath = response;
        console.log('Operação realizada com sucesso!')
      },
      //catch
      error => { console.log('Erro ao salvar!') })
      //finally
      .add(() => {
        this.closeEdit();
      });
  }

}
