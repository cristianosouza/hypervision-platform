interface Timeline {
    currentTimeColor: string,
    graphBackgroundColor: string,
    resumeBackgroundColor: string,
    fontColor: string,
    fontSize: number,
    fontFamily: string,
    heigth: number,
    initialLimit: number,
    finalLimit: number
}

export { Timeline };