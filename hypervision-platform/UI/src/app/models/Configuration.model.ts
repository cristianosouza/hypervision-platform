import { SeverityType } from './SeverityType.model';
import { Timeline } from './Timeline.model';
import { EventFeed } from './EventFeed.model';

interface Configuration {
    categoryCardConfigurations?: Event[];
    timelineConfigurations?: Timeline;
    eventFeedConfigurations?: EventFeed;
}

interface Event {
    category: SeverityType; 
    categoryName: string; 
    notificationSound: string;
    color: string;
    name: string;
    repeatNotificationSound: boolean;
}

export { Configuration, Event };
