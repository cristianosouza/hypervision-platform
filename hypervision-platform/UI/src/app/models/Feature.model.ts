import { FeaturePermissions } from './FeaturePermissions.model';

interface Feature {
  name: string;
  permission: FeaturePermissions;
}
export { Feature };
