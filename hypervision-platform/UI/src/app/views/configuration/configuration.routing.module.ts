import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfigurationComponent } from './configuration.component';

const routes: Routes = [
    {
      path: '',
      component: ConfigurationComponent,
      children: [
        {
          path: 'EventManagement',
          loadChildren: () => import('../../components/event-management/event-management.module').then(m => m.EventManagementModule)
        },
        {
          path: 'LogManagement',
          loadChildren: () => import('../../components/log-management/log-management.module').then(m => m.LogManagementModule)
        },
        {
          path: 'NavbarManagement',
          loadChildren: () => import('../../components/navbar-management/navbar-management.module').then(m => m.NavbarManagementModule)
        },
        {
          path: 'TimelineManagement',
          loadChildren: () => import('../../components/timeline-management/timeline-management.module').then(m => m.TimelineManagementModule)
        },
        {
          path: 'FeedManagement',
          loadChildren: () => import('../../components/feed-management/feed-management.module').then(m => m.FeedManagementModule)
        },
        { path: '', redirectTo: '/configuration/EventManagement', pathMatch: 'full' }
      ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationRoutingModule {}
export const RoutingComponents = [];
