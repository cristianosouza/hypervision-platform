import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfigurationRoutingModule } from './configuration.routing.module';
import { ConfigurationComponent } from './configuration.component';
import { ConcertLoadingIoModule } from '@xst/concert-loading-io';
import { ConcertTableIoModule } from '@xst/concert-table-io';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DrawerComponent } from '../../components/drawer/drawer.component';
import { ConcertNavbarIoModule } from '@xst/concert-navbar-io';
import { NavbarManagementModule } from 'src/app/components/navbar-management/navbar-management.module';
import { TimelineManagementModule } from './../../components/timeline-management/timeline-management.module';
import { LogManagementModule } from './../../components/log-management/log-management.module';
import { EventManagementModule } from '../../components/event-management/event-management.module';
import { FeedManagementModule } from './../../components/feed-management/feed-management.module';

@NgModule({
  declarations: [
    ConfigurationComponent,
    DrawerComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ConfigurationRoutingModule,
    ConcertLoadingIoModule,
    ConcertTableIoModule,
    ReactiveFormsModule,
    ConcertNavbarIoModule,
    EventManagementModule,
    LogManagementModule,
    NavbarManagementModule,
    TimelineManagementModule,
    FeedManagementModule
  ]
})
export class ConfigurationModule { }
