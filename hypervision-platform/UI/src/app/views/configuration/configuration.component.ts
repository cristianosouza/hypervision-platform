import { Component, OnInit, Input, TemplateRef, ViewChild } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { Title } from '@angular/platform-browser';
import { NavbarConfig } from '@xst/concert-navbar-io';
import { Configuration, Event } from '../../models/Configuration.model';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit {
  urls = [];
  @Input() public config: Configuration;

  navbarSettings: NavbarConfig;

  @ViewChild('modalSave') private modalSave: TemplateRef<any>;

  constructor(private title: Title,
    private services: CommonService,
  ) { }

  ngOnInit() {
    this.title.setTitle("Plataforma Hypervision");
    this.services.checkIfIsLogged();
    this.setNavbar();
    this.services.get(environment.endPoints.getUrls).subscribe(d => {
      this.urls = d;
    }, err => {
      console.error(err);
    });
  }

  setNavbar() {
    this.navbarSettings = {
      backgroundColor: '#111619',
      showNotification: false,
      mainLogo: {
        image: '../../../assets/img/cemig-logo.png',
        name: 'Plataforma Hypervision',
        fontSize: 27
      },
      userInfo: {
        //photo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABF1BMVEX////m5ubtuYr1l5fQ0NDVpnxNTU1HOCrtuIjInHT/T23stID2lpjstYPy1r3aqn/++/heSjdXST31kpL0v45UQjHtvIng4OBQPy/V1dXksoU5OTn69vHtwZn1j4/rlpLpj5M2KyA0NiSwRE/ZsY3y5OW7km39+PP669/34M3xyKXz0LPx7uvgxrHWzsjswcLv0dLrsLHrnJ3jpYrmmo7krIllTzuSclXioIv3xZk8QUaAgIDz28bBwcE1NTVaWlrnS2T/RGvtm3/ay8Dxra7ptbfsrK3xz9D23t/vnZOffV17YUh2XUXqvq1kaWxycnK4sKmPhHo1IQczHwBZOjApLB3NcWbsjHn1aXHqqIPyfXf6VG3kw6fws/vQAAALPElEQVR4nO2cC1vayBrHQxAJhQguASRybdV6QVRE0Vp3163aVj27Pd1L191+/89xZhJyg5lJgAnvi4f/83QFGXjmx/+9zEziKspSSy211FJLoVFh+/jo7uTkttfrndwdHW8XoCckU53ju16y0ShSJYnoz0axd3cMPTEp6hyftBo22KgI5u3xglvZOeqx4TzI4sk29CynVuFjryHEG0I2bheTcfte7F6A8X7xYvVTJPs8xuQn6BlPosI2Cc8J8Cw1FqSubh/d93iVMwxxEVzcvp0OznMReTreTZR64yr2SPPofYTG4KrQnI3PtbLXgUZhSxYgUREnYk8aYDLZg4Zh6UgiYLKBcI1TkAmI0kSpFibpGqeFbEHekgtoQTZOoKl86ky8SIvEiChYP0kOUgcRj4sf4yFEVFTjIizeQpM5Oo6JMFnEshTfjo0QzZYqNkI0O437uBLxCJrM0adYGiImQqUXEyGaKFW24zERT0MkLTEWxCY0ll9Tby9yOe5LiNKQajoTc7n+WZNLiKYdUp1Mhdc8VYmaHBtReThFv6D2lSmgWj5Nshlb0Fg+TY7XvDBtPirzgslYxHPWH6gzrWZLUD4oHcE7K3t8li5Yb2lBgzkKnEQ1iTen/SbhCPiSs9ByyWb/4swcxbN8ZFQcNFcz/BY2TSu1yubZRf9zszVUs/m53784PTOZcLbOxl0s3kOjDeW30PSmXA6KR+YOZ5hYrEGzWfLt8XNnYRwCsUzEUWu8dXfudAZAtcyoNijC1Ft25/qhoShG7I8hFqHpqLw605wNkIWIYnvhBmnODGcIQzwdaaUYtogdx8LcxcyARGbQRgzHwu5Z4swx6jAGugaCs/27ooRGEUQMuAjN56XhjHXUL3+gNuBPhRvSyowrf+uHL6bDS2tyysxQ/jCF3+gPj/RlWhhYocKv2+xSmutLBAyEKXxDtFc0Ui0kaqIjlNULHZ26JsIfR1ntcLY9BUstPITWdSfZQeo7tkFCKDtIVS8T4fPw3mqG8gmdTITvFj2pS1JPTk+EJ7yPJQ1VtyfCHyhaeRgDoGNiA/x2U0oYQ6FRHRPh7zg5iaeUqu6hDTQg7fi5zwxCwyB7hHzeNK1HHJGXTDpIZQ6ythjwe3yyahs/RDTMzMPjeWmDqLT++JDiAKYeHtftQeePDxl1FLLcT2I4MKWEwXZoqJnHjVJp3VUpza61Zto/qLTxmBlxkmQi/JKGHukHd7/GznnJnrIrPqEn6y3nO0HEHIIlDb251E9o5J/oZDfOBzsp06B5ltoZZDiZmKGDSAoadND5BmV8yvvHNhEcYtAzfR+hsVOi7g1SvtIhrjTeo9SAfjcl/9dBCKH5FHoS5REaO8SIjYHJZxLJMAf07b5IbSUx3GLa8hFmaKTxYjIKY4ZGeMYjRFBoFOW26BIaT2SCtoG6fllPbV3quhYCpZGRW6k6GWnbSD7hyf2KWkX4NFQK/ym6O3xjsFHKW3zq+92rd+/e/drdq+tCQL2+1/2VjLzafa/SkUaqtDHwCJtr4Iu2PV078x1hZKy+kF1TlFVLmxU6gm+gtqdUNu2hirKWtVz0glQ9M3V9DxbwIMvyZVdRrlZXnYm/VXmImvrW+SpWV68UZZflN/264LTGAswSwMLQmNXNKnmisRE1jYRg1R1JnuwyP+89HGCN6SANq4Q772uFM3H7q1Cu3ZEJhQY9axzcDRn7DG+0On3F83CFPj1gmagd0JdW/B4qSp31kQdQgGwLd63XfvPykKjAska3y6Qz8DfrGTMVdaiCypyNZr82DNMfr+2nDLe1fful6x+9ICViEu7OH87SGmM2bkRVftrc3FwdArLyy20D16tk5E+V4TNWQIN1jDXWZLzCV7muuo8ZdvuMqV5X3MfvGV+bDtUwmITsgGKkbPYtcySrmmoLSsjuAazkxkXIbs/iKPWLFaWoCDm9S1RpgmJVGlSETrcYkaBbjIj5magImcEn6vhBsXssKkJ71TYiwaptRKxVGzJCVn6JVt5BMVfe2AjHJy7cPQXE+SqwEapaELEm2AEHeyIzCTESkm25zxzhKYY/pAvMAwNQQlZztqVra7Y7b/cuQ0+i7NVbbU3jfxzULp8XVNaksvrlfl3LRjhNzGr1/UsyXvBhULunAi+qhnPnFJjJR4LtgJmnGDGIs/qZhzjFnaX8uCK/F/AkirnLZyszrsiAoAem3AI/qtQYIO/S9xgg2EmbrffZiLk4nYUa+Kk+7dNqtFCdBlBX4a/MUIn6ok95H1+0MgPWB0dVi1pvTAsylY96GxxcHxxVbG0RGswVc9s6u5ibaRhxNxkzEoI2woAilppJhabQcC5CzS7I1dqoYklERGkYkoiGoWU5EtwuhSoNxYlonm98UQpMKf9dF7R+RGlIJHBw0G5f8d72Q/tR4OI8AULFD1NjkE5/rbLf9eVre8AlBLtoyBZ/4WbstAniNetNX35Pt3f4hIgqKRX/QMMkhOmv6R/GRH6ZbnOXqIBHF2zxDzSM8zRfT1wLGUf+wOK2RCtMOWrvcC3E1AxtCUwscQnXF8hCUSamLBNfv34d+Ect5J7VoMtCKv7i1Hhop1+n3/z86tWrn9/8MfxJftN+4FuIrJDa4h8tGoM/37wa1Zs/+d0eWS90xb2Kppb/uhkjvPmL+9dSCMuMLcEmqvxtFPHmG//PwbA1e0/8y21q+flvP+PN32cCQPAjUr5El2rKz//8e2Pr33+eBX/Pp2Oso66EW+Fy2Xz+5dsvz4L/K52KOAlt8W5K8GGGvK6paA5J2apFvizKA0RbZRzVQq9rCwE19ICim0tehINUhakR9TryHHS1P90JcRZ1mwhqLep1U580sLtmptJuxMumnvT6QqSgTwcT1VQN625CpFo9cqhq2YUz0NZuxMsZWh3hkUVEbUVBNLegpzmDtsKv2Zup1GITEgkgzTwdsPCEFHKc0rTpXgqhw5k3qcjPwO9fDiFPS0LMqmtmKJ+J/NhCLNryzbwAj1aghScM1s0ROntJAz3NGeRftlk1lFRR67+mr3u8GEKuloSo9ZIJC4VarVaISGiNXZBDKAJWqXS7K7qtiPtDe3C23u1WKh3MpLVOgmiFKhLZmJf0nVXyER2MO/5Cp1NJJGYj/O4QUkhkXnYSnizCaf4iSjv0EVqU0FiOCn6+RKJK53k4xZmw3h0hTOBIylpiRBbhyhQeWmm4Ugl+GnhKjvGRL96a6PeJEfUP1hvHPg+UcSQ+/Yk4sYl2JQ0EqcsIFqtMPidMP0yYidoKK0iHYQFTcwpsPtfEyYrNMEZZFloCsJFjoPWVT444BGRbaGneNvINpKpOiKipQ0CuhYl5ZyOjhAZkz3elG+1qsF4fjh8vpAHNEVEQobYqzpS/hy++NfUwGmAiMbe+EQroQ+yGMOqayxcKOLdkjADoQySMKg9S0+seXwTAOSFGAky45cbqjQRSD95KpGm6Vj/semNERWa+iFEBA4jEyQ/f62pWd1TfP/zgf1nQJuaNGB1wlHFISjT+24gGzgNxIkB/NopUjWxg/IgTAlJGho8z8SVibRrilcw0kJPjUcXX+qcCtCEZlNXp8KjiApw8RgOUFcLpqDI1nKWYUnE2QLmKJRWnS8K4FEcqQjMFFUOcYopRKvlxCk00JtmA2CyUHqe4yowtuYT4LJRsIkYL5XYMjBbKNRGahSN5gDgtlGkiNAlXsgDDzn/hJKvWYA1SeWEKzSGQHEC8QSorTPEGqawwhaYQSgYgzhWbIxlhijlI5WyEcRPKSERohhAtCcOFu9D8PxDOXmpwFxoZ7QI74ezF9OUTQhOE6eUTzl5MoQFCtSRcEi4J4bUkXBIuCeG1JPwf8el0y+rF7QEAAAAASUVORK5CYII=',
        name: this.services.user.name
      },
      applicationsList: [
        {
          name: "Gestão de Usuários",
          url: environment.authentication + "UserManagement",
          icon: "fa fa-user",
          target: "_blank",
          hint: "Gestão de usuários"
        },
        {
          name: " Meteorologia ",
          url: this.urls[0],
          icon: "fa fa-cloud",
          target: "_blank"
        },
        {
          name: " Queimadas ",
          url: this.urls[1],
          icon: "fa fa-fire",
          target: "_blank"
        },
        {
          name: " Configurações ",
          url: "/configuration",
          icon: "fa fa-cog",
          target: "_self"
        }
      ],
      filters: {
        multiple: true,
        name: "Atuação",
        data: ["Aguarde..."]
      }
      //user: this.services.user.name

    };
    this.services.getFields((a) => { this.navbarSettings.filters.data = a; });
  }

  logout() {
    this.services.logout();
  }

  applyFilters(filter) {
    // this.services.setToken(this.user, filter);
  }
  
}

