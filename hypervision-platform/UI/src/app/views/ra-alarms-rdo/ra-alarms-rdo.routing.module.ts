import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RAAlarmsRdoComponent } from './ra-alarms-rdo.component';

const routes: Routes = [
    {
        path: '',
        component: RAAlarmsRdoComponent,
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RAAlarmsRDORoutingModule {}
