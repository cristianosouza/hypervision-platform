//import { EventManagementComponent } from './../../components/event-management/event-management.component';
import { Component, OnInit, ViewChild, TemplateRef, ViewEncapsulation } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subject, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';
import { ActionType, Card, CardAction, CardfeedConfig, CardLevelType, CardLevelStandard, ConcertCardfeedIoComponent, Dashboard } from '@xst/concert-cardfeed-io';
import { ConcertTimelineIoService, Event, TimelineConfig } from '@xst/concert-timeline-io';
import { NavbarConfig } from '@xst/concert-navbar-io';
import { Title } from '@angular/platform-browser';
import { CommonService } from 'src/app/services/common.service';
import { SignalRService } from 'src/app/services/signal-r.service';
import { environment } from 'src/environments/environment';
import { UserActionType } from 'src/app/models/Cards.model';
import { UtilitiesService } from '../../services/utilities.service';
import { UserIdleService } from 'angular-user-idle';
import { SeverityType } from '../../models/SeverityType.model';
import { EventType } from 'src/app/models/EventType';
import { userInfo } from 'os';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class HomeComponent implements OnInit {
  @ViewChild('instance', { static: true }) instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();

  urls = [];
  //user: any;
  userFilters: any;
  severityTypes: any[] = [
    { type: SeverityType.INFO, value: "Informação" },
    { type: SeverityType.NOTIFICATION, value: "Notificação" },
    { type: SeverityType.ALERT, value: "Alerta" },
    { type: SeverityType.ALARM, value: "Alarme" },
  ];
  levels = [
    {
      // type: CardLevelType.Success
      type: this.severityTypes[0].value,
      category: this.severityTypes[0]
    },
    {
      // type: CardLevelType.Info
      type: this.severityTypes[1].value,
      category: this.severityTypes[1]
    },
    {
      // type: CardLevelType.Alert,
      type: this.severityTypes[2].value,
      category: this.severityTypes[2],
      sound: {
        src: "../../../assets/audio/alarme.mp3",
        loop: false
      }
    },
    {
      // type: CardLevelType.Alarm,
      type: this.severityTypes[3].value,
      category: this.severityTypes[3],
      sound: {
        src: "../../../assets/audio/sirene.mp3",
        loop: true
      }
    }
  ];
  timelineConfig: TimelineConfig;
  navbarSettings: NavbarConfig;
  timelineConfigDefault: TimelineConfig = {
    heigth: 150,
    currentTimeColor: "#0c1469",
    fontColor: "#65707a",
    fontFamily: "Roboto",
    fontSize: 13,
    graphBackgroundColor: "#273238",
    resumeBackgroundColor: "#aaaaaa",
    initialLimit: 1,
    finalLimit: 7
  };
  eventFeedConfig: CardfeedConfig;
  eventFeedConfigDefault: CardfeedConfig = {
    feedBackgroundColor: "#111619",
    fontColor: "#ffffff",
    cardBackgroundColor: "#090a0c",
    feedWidth: "29%",
    dashboardBackgroundColor: "#111619",
    levels: this.levels,
    displayDateTime: true,
    displayFilter: true,
    allowMultiselectionCard: true,
    soundAlert: false,
    allowHideCard: true,
    orderBySeverity: true,
    orderByProperty: "dateStart",
    descendingOrder: true
  };

  cards: Card[] = [];
  activeCard: Card;
  modalTitle: string;
  modalComment: boolean;
  modalInformation: boolean;
  modalFirstInformationText: string;
  modalSecondInformationText: string;
  modalConfirm: boolean;
  modalSelect: boolean;
  modalMessage: string;
  modalRemove;
  eventsToRemove: Card[] = [];
  eventsToForward: Card[] = [];
  userList: string[] = [];
  receiverUser: string;
  hiddenEvents: Card[] = [];
  multiCardButtons: CardAction[] = [];
  inactivatedUser = false;
  //maxUserDowntime = 900; // segundos
  actionCardComment: string = "";
  acceptedEvent: Card;

  @ViewChild('modalConfirmation') private modalConfirmation: TemplateRef<any>;
  @ViewChild(ConcertCardfeedIoComponent) private childCardFeedComponent: ConcertCardfeedIoComponent;

  constructor(private title: Title,
    private services: CommonService,
    private signalr: SignalRService,
    private timeline: ConcertTimelineIoService,
    private modalService: NgbModal,
    private userIdle: UserIdleService) { }

  ngOnInit(): void {
    this.title.setTitle("Plataforma Hypervision");
    this.services.checkIfIsLogged();
    this.setNavbar();
    this.getConfiguration();
    this.getUser();
    this.getMultiCardButtons();
    this.verifyExpiredCards();
    this.services.get(environment.endPoints.getUrls).subscribe(d => {
      this.urls = d;
      this.signalr.startConnection(this.addOrUpdateCard, (d) => { });
    }, err => {
      console.error(err);
    });

    this.initializeUserDowntimeConfig();
  }

  initializeUserDowntimeConfig() {
    let token = this.services.getAccesToken();
    let creationDate = new Date(token.creationDate);
    let expirationDate = new Date(token.expirationDate);
    this.userIdle.setConfigValues({ idle: (expirationDate.getTime() - creationDate.getTime()) / 1000 });
    //Start watching for user inactivity.
    this.startWatching();

    // Start watching when user idle is starting.
    this.userIdle.onTimerStart().subscribe(count => {

      if (this.inactivatedUser) {
        return;
      }

      this.inactivatedUser = true;
      this.stopWatching();
      this.stop();

      this.resetModalConfigurations();
      this.modalTitle = "Você foi desconectado...";
      this.modalInformation = true;
      this.modalFirstInformationText = 'Você foi desconectado por inatividade.';
      this.modalSecondInformationText = 'Refaça o login para acessar o sistema novamente!';

      var modalRef = this.modalService.open(this.modalConfirmation, { centered: true, backdrop: "static", keyboard: false, windowClass: 'modal-without-close-button' });
      modalRef.result.then((result) => {
        this.logout()
      }).catch((error) => {
        if (error) console.log(error);
      });

    });
  }

  stop() {
    this.userIdle.stopTimer();
  }

  stopWatching() {
    this.userIdle.stopWatching();
  }

  startWatching() {
    this.userIdle.startWatching();
  }

  setNavbar() {
    this.navbarSettings = {
      backgroundColor: '#111619',
      showNotification: false,
      mainLogo: {
        image: '../../../assets/img/cemig-logo.png',
        name: 'Plataforma Hypervision',
        fontSize: 27
      },
      userInfo: {
        //photo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABF1BMVEX////m5ubtuYr1l5fQ0NDVpnxNTU1HOCrtuIjInHT/T23stID2lpjstYPy1r3aqn/++/heSjdXST31kpL0v45UQjHtvIng4OBQPy/V1dXksoU5OTn69vHtwZn1j4/rlpLpj5M2KyA0NiSwRE/ZsY3y5OW7km39+PP669/34M3xyKXz0LPx7uvgxrHWzsjswcLv0dLrsLHrnJ3jpYrmmo7krIllTzuSclXioIv3xZk8QUaAgIDz28bBwcE1NTVaWlrnS2T/RGvtm3/ay8Dxra7ptbfsrK3xz9D23t/vnZOffV17YUh2XUXqvq1kaWxycnK4sKmPhHo1IQczHwBZOjApLB3NcWbsjHn1aXHqqIPyfXf6VG3kw6fws/vQAAALPElEQVR4nO2cC1vayBrHQxAJhQguASRybdV6QVRE0Vp3163aVj27Pd1L191+/89xZhJyg5lJgAnvi4f/83QFGXjmx/+9zEziKspSSy211FJLoVFh+/jo7uTkttfrndwdHW8XoCckU53ju16y0ShSJYnoz0axd3cMPTEp6hyftBo22KgI5u3xglvZOeqx4TzI4sk29CynVuFjryHEG0I2bheTcfte7F6A8X7xYvVTJPs8xuQn6BlPosI2Cc8J8Cw1FqSubh/d93iVMwxxEVzcvp0OznMReTreTZR64yr2SPPofYTG4KrQnI3PtbLXgUZhSxYgUREnYk8aYDLZg4Zh6UgiYLKBcI1TkAmI0kSpFibpGqeFbEHekgtoQTZOoKl86ky8SIvEiChYP0kOUgcRj4sf4yFEVFTjIizeQpM5Oo6JMFnEshTfjo0QzZYqNkI0O437uBLxCJrM0adYGiImQqUXEyGaKFW24zERT0MkLTEWxCY0ll9Tby9yOe5LiNKQajoTc7n+WZNLiKYdUp1Mhdc8VYmaHBtReThFv6D2lSmgWj5Nshlb0Fg+TY7XvDBtPirzgslYxHPWH6gzrWZLUD4oHcE7K3t8li5Yb2lBgzkKnEQ1iTen/SbhCPiSs9ByyWb/4swcxbN8ZFQcNFcz/BY2TSu1yubZRf9zszVUs/m53784PTOZcLbOxl0s3kOjDeW30PSmXA6KR+YOZ5hYrEGzWfLt8XNnYRwCsUzEUWu8dXfudAZAtcyoNijC1Ft25/qhoShG7I8hFqHpqLw605wNkIWIYnvhBmnODGcIQzwdaaUYtogdx8LcxcyARGbQRgzHwu5Z4swx6jAGugaCs/27ooRGEUQMuAjN56XhjHXUL3+gNuBPhRvSyowrf+uHL6bDS2tyysxQ/jCF3+gPj/RlWhhYocKv2+xSmutLBAyEKXxDtFc0Ui0kaqIjlNULHZ26JsIfR1ntcLY9BUstPITWdSfZQeo7tkFCKDtIVS8T4fPw3mqG8gmdTITvFj2pS1JPTk+EJ7yPJQ1VtyfCHyhaeRgDoGNiA/x2U0oYQ6FRHRPh7zg5iaeUqu6hDTQg7fi5zwxCwyB7hHzeNK1HHJGXTDpIZQ6ythjwe3yyahs/RDTMzMPjeWmDqLT++JDiAKYeHtftQeePDxl1FLLcT2I4MKWEwXZoqJnHjVJp3VUpza61Zto/qLTxmBlxkmQi/JKGHukHd7/GznnJnrIrPqEn6y3nO0HEHIIlDb251E9o5J/oZDfOBzsp06B5ltoZZDiZmKGDSAoadND5BmV8yvvHNhEcYtAzfR+hsVOi7g1SvtIhrjTeo9SAfjcl/9dBCKH5FHoS5REaO8SIjYHJZxLJMAf07b5IbSUx3GLa8hFmaKTxYjIKY4ZGeMYjRFBoFOW26BIaT2SCtoG6fllPbV3quhYCpZGRW6k6GWnbSD7hyf2KWkX4NFQK/ym6O3xjsFHKW3zq+92rd+/e/drdq+tCQL2+1/2VjLzafa/SkUaqtDHwCJtr4Iu2PV078x1hZKy+kF1TlFVLmxU6gm+gtqdUNu2hirKWtVz0glQ9M3V9DxbwIMvyZVdRrlZXnYm/VXmImvrW+SpWV68UZZflN/264LTGAswSwMLQmNXNKnmisRE1jYRg1R1JnuwyP+89HGCN6SANq4Q772uFM3H7q1Cu3ZEJhQY9axzcDRn7DG+0On3F83CFPj1gmagd0JdW/B4qSp31kQdQgGwLd63XfvPykKjAska3y6Qz8DfrGTMVdaiCypyNZr82DNMfr+2nDLe1fful6x+9ICViEu7OH87SGmM2bkRVftrc3FwdArLyy20D16tk5E+V4TNWQIN1jDXWZLzCV7muuo8ZdvuMqV5X3MfvGV+bDtUwmITsgGKkbPYtcySrmmoLSsjuAazkxkXIbs/iKPWLFaWoCDm9S1RpgmJVGlSETrcYkaBbjIj5magImcEn6vhBsXssKkJ71TYiwaptRKxVGzJCVn6JVt5BMVfe2AjHJy7cPQXE+SqwEapaELEm2AEHeyIzCTESkm25zxzhKYY/pAvMAwNQQlZztqVra7Y7b/cuQ0+i7NVbbU3jfxzULp8XVNaksvrlfl3LRjhNzGr1/UsyXvBhULunAi+qhnPnFJjJR4LtgJmnGDGIs/qZhzjFnaX8uCK/F/AkirnLZyszrsiAoAem3AI/qtQYIO/S9xgg2EmbrffZiLk4nYUa+Kk+7dNqtFCdBlBX4a/MUIn6ok95H1+0MgPWB0dVi1pvTAsylY96GxxcHxxVbG0RGswVc9s6u5ibaRhxNxkzEoI2woAilppJhabQcC5CzS7I1dqoYklERGkYkoiGoWU5EtwuhSoNxYlonm98UQpMKf9dF7R+RGlIJHBw0G5f8d72Q/tR4OI8AULFD1NjkE5/rbLf9eVre8AlBLtoyBZ/4WbstAniNetNX35Pt3f4hIgqKRX/QMMkhOmv6R/GRH6ZbnOXqIBHF2zxDzSM8zRfT1wLGUf+wOK2RCtMOWrvcC3E1AxtCUwscQnXF8hCUSamLBNfv34d+Ect5J7VoMtCKv7i1Hhop1+n3/z86tWrn9/8MfxJftN+4FuIrJDa4h8tGoM/37wa1Zs/+d0eWS90xb2Kppb/uhkjvPmL+9dSCMuMLcEmqvxtFPHmG//PwbA1e0/8y21q+flvP+PN32cCQPAjUr5El2rKz//8e2Pr33+eBX/Pp2Oso66EW+Fy2Xz+5dsvz4L/K52KOAlt8W5K8GGGvK6paA5J2apFvizKA0RbZRzVQq9rCwE19ICim0tehINUhakR9TryHHS1P90JcRZ1mwhqLep1U580sLtmptJuxMumnvT6QqSgTwcT1VQN625CpFo9cqhq2YUz0NZuxMsZWh3hkUVEbUVBNLegpzmDtsKv2Zup1GITEgkgzTwdsPCEFHKc0rTpXgqhw5k3qcjPwO9fDiFPS0LMqmtmKJ+J/NhCLNryzbwAj1aghScM1s0ROntJAz3NGeRftlk1lFRR67+mr3u8GEKuloSo9ZIJC4VarVaISGiNXZBDKAJWqXS7K7qtiPtDe3C23u1WKh3MpLVOgmiFKhLZmJf0nVXyER2MO/5Cp1NJJGYj/O4QUkhkXnYSnizCaf4iSjv0EVqU0FiOCn6+RKJK53k4xZmw3h0hTOBIylpiRBbhyhQeWmm4Ugl+GnhKjvGRL96a6PeJEfUP1hvHPg+UcSQ+/Yk4sYl2JQ0EqcsIFqtMPidMP0yYidoKK0iHYQFTcwpsPtfEyYrNMEZZFloCsJFjoPWVT444BGRbaGneNvINpKpOiKipQ0CuhYl5ZyOjhAZkz3elG+1qsF4fjh8vpAHNEVEQobYqzpS/hy++NfUwGmAiMbe+EQroQ+yGMOqayxcKOLdkjADoQySMKg9S0+seXwTAOSFGAky45cbqjQRSD95KpGm6Vj/semNERWa+iFEBA4jEyQ/f62pWd1TfP/zgf1nQJuaNGB1wlHFISjT+24gGzgNxIkB/NopUjWxg/IgTAlJGho8z8SVibRrilcw0kJPjUcXX+qcCtCEZlNXp8KjiApw8RgOUFcLpqDI1nKWYUnE2QLmKJRWnS8K4FEcqQjMFFUOcYopRKvlxCk00JtmA2CyUHqe4yowtuYT4LJRsIkYL5XYMjBbKNRGahSN5gDgtlGkiNAlXsgDDzn/hJKvWYA1SeWEKzSGQHEC8QSorTPEGqawwhaYQSgYgzhWbIxlhijlI5WyEcRPKSERohhAtCcOFu9D8PxDOXmpwFxoZ7QI74ezF9OUTQhOE6eUTzl5MoQFCtSRcEi4J4bUkXBIuCeG1JPwf8el0y+rF7QEAAAAASUVORK5CYII=',
        name: this.services.user.name
      },
      applicationsList: [],
      filters: {
        multiple: true,
        name: "Atuação",
        data: ["Aguarde..."]
      }
      //user: this.services.user.name

    };
    // this.navbarSettings = navbarConfigurations;
    // this.navbarSettings.filters.data = ["Aguarde..."];
    // this.navbarSettings.userInfo.name = this.services.user.name;
    // this.navbarSettings.applicationsList = [];

    this.addNavbarApplications();
    this.services.getFields((a) => { this.navbarSettings.filters.data = a; });
  }

  addNavbarApplications() {
    //if (this.services.userHasFeatureAccess("ConfigurationHypervision", "ManagerUsersAndGroups", "view")) {
    this.navbarSettings.applicationsList.push({
      name: "Gestão de Usuários",
      url: environment.authentication + "UserManagement",
      icon: "fa fa-user",
      target: "_blank",
      hint: "Gestão de usuários"
    });
    //}

    this.navbarSettings.applicationsList.push({
      name: " Meteorologia ",
      url: this.urls[0],
      icon: "fa fa-cloud",
      target: "_blank"
    });

    //Criar feature de visualização do módulo no geral
    this.navbarSettings.applicationsList.push({
      name: " Queimadas ",
      url: this.urls[1],
      icon: "fa fa-fire",
      target: "_blank"
    });

    //if (this.services.userHasFeatureAccess("ConfigurationHypervision", "GetConfiguration", "view")) {
    this.navbarSettings.applicationsList.push({
      name: " Configurações ",
      url: "/configuration",
      icon: "fa fa-cog",
      target: "_self"
    });
    //}
  }

  logout() {
    this.services.logout();
  }

  addNewCard(card: Card, message: any) {
    if (message.eventEnd && new Date(message.eventEnd) < new Date()) {
      this.finishExpiredEvent(card);
      return;
    }
    this.addAcceptButton(card, false, false);
    this.addRejectButton(card, false, false);
    //this.addDeleteButton(card, true, false);
    this.addCard(card);
  }

  addOrUpdateCard = (message: any) => {
    let title = "";
    let level: any;
    let dashboard: Dashboard[];
    let resume: any[];
    let lastEventForUser: any;

    if (message.eventHistory) {
      lastEventForUser = message.eventHistory.find(e => {
        return this.services.user.user == e.user
      });
      if (!lastEventForUser) {
        lastEventForUser = message.eventHistory[0];
      }
    } else {
      lastEventForUser = message.lastEvent;
    }

    switch (message.eventType) {
      case EventType.Meteorological:
        level = this.levels[1];
        title = "Aviso meteorológico " + message.level;
        dashboard = [{
          id: "Aplicação",
          type: "iframe",
          url: this.urls[0],
          width: "100%",
          height: "100%",
          title: title
        }];
        resume = [
          `Linhas em alerta: ${message.cardDetails.lines?.join(",")}`,
          `Subestações em alerta: ${message.cardDetails.substations?.join(",")}`,
          `Regiões: ${message.cardDetails.regions?.join(",")}`
        ];
        break;
      case EventType.Fire:
        title = "Alerta de queimada " + message.level;
        level = this.levels[2];
        dashboard = [
          {
            id: "Queimadas",
            type: "iframe",
            url: this.urls[1],
            width: "100%",
            height: "100%",
            title: "Queimadas"
          }];
        resume = [
          `Linhas em alerta: ${message.cardDetails.lines?.join(",")}`,
          `Subestações em alerta: ${message.cardDetails.substations?.join(",")}`,
          `Regiões: ${message.cardDetails.regions?.join(",")}`
        ];
        if (message.lastEvent.type == UserActionType.Forward && message.lastEvent.receiverUser == this.services.user.user ||
          message.lastEvent.type > UserActionType.None && message.lastEvent.user == this.services.user.user) {
          dashboard = [
            {
              id: "Aplicação",
              type: "iframe",
              url: "/rdo/" + message.id,
              width: "100%",
              height: "100%",
              title: "Ocorrência"
            },
            {
              id: "Queimadas",
              type: "iframe",
              url: this.urls[1],
              width: "100%",
              height: "100%",
              title: "Queimadas"
            },
            {
              id: "Equipes",
              type: "iframe",
              url: this.urls[2],
              width: "100%",
              height: "100%",
              title: "Equipes"
            }];
        }
        break;
      case EventType.TagMeasurement:
        level = this.levels[2];
        title = "Medição suspeita - TAG: " + message.cardDetails.tag;
        resume = [
          `Subestação: ${message.cardDetails.tagRem}`,
          `Tag: ${message.cardDetails.tag}`,
          `Valor do alarme: ${message.cardDetails.alarmValue}`,
          `Valor previsto: ${message.cardDetails.predictedAlarm}`
        ];
        dashboard = [{
          id: "Aplicação",
          type: "iframe",
          url: "/measurement/" + message.id,
          width: "100%",
          height: "100%",
          title: "Validação"
        }];
        break;
      case EventType.RA:
        level = this.levels[2];
        title = "Religamento Automático " + message.level;
        resume = [`Estado: ${message.cardDetails.status}`];
        if (message.cardDetails.regions != null) {
          resume.push(`Regiões: ${message.cardDetails.regions?.join(",")}`);
        }
        dashboard = [
          {
            id: "Alarmes",
            type: "iframe",
            url: "/raalarmsrdo/" + message.id,
            width: "100%",
            height: "100%",
            title: "Alarmes"
          },
          {
            id: "Aplicação",
            type: "iframe",
            url: "/rardo/" + message.id,
            width: "100%",
            height: "100%",
            title: "Religamento Automático"
          }
        ];
        break;
      case EventType.Fault:
        level = this.levels[3];
        title = "Falta - Subestações: " + message.level;
        dashboard = [{
          id: "Atuações",
          type: "iframe",
          url: "/fault/" + message.id,
          width: "100%",
          height: "100%",
          title: title
        }];
        let openEquipments = message.cardDetails.tag.split(",").map(_=> {return _.replace("_ED", "")});


        resume = [
          `Equipamentos abertos: ${openEquipments.join(", ")}`
        ]
        if (message.lastEvent.type == UserActionType.Forward && message.lastEvent.receiverUser == this.services.user.user ||
          message.lastEvent.type > UserActionType.None && message.lastEvent.user == this.services.user.user) {
          dashboard = [
            {
              id: "App",
              type: "iframe",
              url: "/fault/" + message.id,
              width: "100%",
              height: "100%",
              title: "RDO"
            },
            {
              id: "OI",
              type: "iframe",
              url: "../../../assets/pdf/io.temp.pdf",
              width: "100%",
              height: "100%",
              title: "Instrução de operação"
            },
            {
              id: "DescargasAtmosfericas",
              type: "iframe",
              url: this.urls[3],
              width: "100%",
              height: "100%",
              title: "Descargas atmosféricas"
            },
            {
              id: "Equipes",
              type: "iframe",
              url: this.urls[2],
              width: "100%",
              height: "100%",
              title: "Equipes"
            }];
        }
        break;
    }

    let card: Card = {
      id: message.id,
      customObject: message,
      dateStart: new Date(message.eventStart),
      dateEnd: message.eventEnd == null ? null : new Date(message.eventEnd),
      fontColor: "#ffffff",
      resume: resume,
      title: title,
      active: this.activeCard == null ? false : true,
      level: level,
      actions: [],
      dashboard: dashboard,
      selected: false,
      activeSound: level.sound ? true : false
    };

    //Caso a notificação seja nova e ninguem tenha executado ação sobre ela.
    //Verifica se o card é novo ou se é uma atualização.
    let index = this.cards.findIndex(c => c.id == message.id);
    if (message.lastEvent.type == UserActionType.None && index <= -1) {
      this.addNewCard(card, message);
      return;
    }

    this.addCardButtons(card, message.severity, lastEventForUser, message.lastEvent);
    let removeCard = this.isToRemoveFromFeed(message.lastEvent, lastEventForUser);

    if (this.cards.length > 0) {
      // let index = this.cards.findIndex(c => c.id == message.id);
      if (index > -1) {
        if (removeCard) {
          this.cards.splice(index, 1);
          // Se for evento com severidade do tipo notificação, deixa na timeline
          this.removeOnTimeLine(card);
          this.childCardFeedComponent.closeDashboard();
        } else {
          this.cards[index] = card;
        }
      } else {
        if (!removeCard) {
          this.addCard(card);
        }
      }
    } else {
      if (!removeCard) {
        this.addCard(card);
      }
    }
    if (this.childCardFeedComponent.displayDashboard && (message.lastEvent.type == UserActionType.Start || message.lastEvent.type == UserActionType.Comment)) {
      this.activeCard = card;
    }
    if (this.activeCard) {
      if (message.id == this.activeCard.id) {
        let index = this.cards.findIndex(c => c.id == this.activeCard.id);
        this.cards[index].active = true;
        this.childCardFeedComponent.openDashboard(this.cards[index]);
        this.activeCard = null;
      }
    }
  }

  addCardButtons(card: Card, severity: number, lastEventForUser: any, lastEvent: any) {
    switch (severity) {
      //NOTIFICAÇÃO
      case SeverityType.NOTIFICATION:
        if (lastEventForUser.type == UserActionType.None) { //Evento sem interação do usuário
          this.addAcceptButton(card, false, false);
          this.addRejectButton(card, false, false);
          this.addDeleteButton(card, true, false);
        } else if (lastEventForUser.type == UserActionType.Accept) { //Evento Aceito
          this.addDeleteButton(card, false, false);
        }
        break;
      //Alerta e Alarme
      case SeverityType.ALERT:
      case SeverityType.ALARM:
        //cartão encaminhado
        if (lastEvent.type == UserActionType.Forward) {
          if (lastEvent.receiverUser == this.services.user.user) {
            this.addAcceptButton(card, false, false);
            return;
          }
        }
        //cartão finalizado
        else if (lastEvent.type == UserActionType.Finish) {
          this.addDeleteButton(card, false, false);
        }
        else { //demais ações
          if (lastEvent.user == this.services.user.user) {
            this.addForwardButton(card, false, false);
          } else {
            this.addRejectButton(card, false, false);
            this.addDeleteButton(card, false, false);
          }
        }
        break;
    }
  }

  addAcceptButton(card: Card, disabled: boolean, hidden: boolean) {
    if (this.services.userHasFeatureAccess("EventHypervision", "AcceptEvent", "edit")) {
      card.actions.push({
        disabled: disabled,
        hidden: hidden,
        type: ActionType.POST,
        url: environment.baseUrl + environment.endPoints.acceptEvent + 1,
        value: "Aceitar",
        btnClass: "btn btn-primary btn-sm",
        callBack: (a: CardAction, c: Card) => {
          this.activeCard = c;
          let url = `${environment.endPoints.acceptEvent}${this.services.user.user}`;
          this.services.update(url, [c.customObject]).subscribe(data => {
          }, err => {
            console.error(err);
          });
        }
      });
    }
  }

  addRejectButton(card: Card, disabled: boolean, hidden: boolean) {
    if (this.services.userHasFeatureAccess("EventHypervision", "RejectEvent", "edit")) {
      card.actions.push({
        disabled: disabled,
        hidden: hidden,
        type: ActionType.POST,
        url: environment.baseUrl + environment.endPoints.rejectEvent + 1,
        value: "Rejeitar",
        btnClass: "btn btn-danger btn-sm",
        callBack: (a: CardAction, c: Card) => {
          this.resetModalConfigurations();
          this.modalComment = true;
          this.modalTitle = "Rejeitar Cartão";
          var modalRef = this.modalService.open(this.modalConfirmation);
          modalRef.result.then((result) => {
            let url = `${environment.endPoints.rejectEvent}${this.services.user.user}/${this.actionCardComment}`;
            this.services.update(url, [c.customObject]).subscribe(data => {
              this.childCardFeedComponent.closeDashboard();
            }, err => {
              console.error(err);
            });
          }).catch((error) => {
            if (error) console.log(error);
          });
        }
      });
    }
  }

  addDeleteButton(card: Card, disabled: boolean, hidden: boolean) {
    if (this.services.userHasFeatureAccess("EventHypervision", "DeleteEvent", "edit")) {
      card.actions.push({
        disabled: disabled,
        hidden: hidden,
        type: ActionType.POST,
        url: environment.baseUrl + environment.endPoints.deleteEvent + 1,
        value: "Remover",
        btnClass: "btn btn-secondary btn-sm",
        callBack: (a: CardAction, c: Card) => {
          this.resetModalConfigurations();
          this.modalTitle = "Confirmação";
          this.modalMessage = "Remover Cartão?";
          this.eventsToRemove.push(c);
          this.modalRemove = this.modalService.open(this.modalConfirmation);
        }
      });
    }
  }

  addForwardButton(card: Card, disabled: boolean, hidden: boolean) {
    if (this.services.userHasFeatureAccess("EventHypervision", "ForwardEvent", "edit")) {
      card.actions.push({
        disabled: disabled,
        hidden: hidden,
        type: ActionType.POST,
        url: environment.baseUrl + environment.endPoints.forwardEvent + card.id,
        value: "Encaminhar",
        btnClass: "btn btn-danger btn-sm",
        callBack: (a: CardAction, c: Card) => {
          this.services.getUsers((users) => {
            this.userList = UtilitiesService.subtractArrays(users, [this.services.user.user]);
            this.eventsToForward.push(c);
            this.forwardEvents();
          });
        }
      });
    }
  }

  addCardInTimelineIfSeverityTypeIsNotification(card: Card) {
    if (card.level.type == CardLevelType.Info) {
      this.addOnTimeLine(card);
    }
  }

  addCard(card: Card) {
    this.cards.push(card);
    this.addOnTimeLine(card);
  }

  // keepTimelineIfSeverityTypeIsNotification(message: any, card: Card) {
  //   if (!(message.lastEvent.type == UserActionType.Accept && card.level.type == CardLevelType.Info)) {
  //     this.removeOnTimeLine(card);
  //   }
  // }

  isExpiredCard(c: Card) {
    if (c.dateEnd != null && c.dateEnd < new Date()) {
      return true;
    }
    return false;
  }

  addOnTimeLine(card: Card) {
    let level = (card.level.color ? card.level.color : CardLevelStandard.get(card.level.type)).toString();
    var e: Event = {
      date: card.dateStart,
      description: card.title,
      id: card.id,
      level: 1,
      color: level
    }
    this.timeline.addEvent(e);
  }

  removeOnTimeLine(card: Card) {
    this.timeline.removeEvent(card.id);
  }

  removeEvents() {
    var events = this.eventsToRemove.map(c => {
      return c.customObject;
    });
    this.modalRemove.result.then((result) => {
      let url = `${environment.endPoints.deleteEvent}${this.services.user.user}`;
      this.services.update(url, events).subscribe(data => {
        this.childCardFeedComponent.closeDashboard();
      }, err => {
        console.error(err);
      });
    }).catch((error) => {
      if (error) console.log(error);
    });
    this.eventsToRemove = [];
  }

  forwardEvents() {
    var events = this.eventsToForward.map(c => {
      return c.customObject;
    });
    this.resetModalConfigurations();
    this.modalSelect = true;
    this.modalTitle = events.length > 1 ? "Encaminhar Cartões" : "Encaminhar Cartão";
    if (this.userList.length > 0) {
      this.modalComment = true;
      // this.receiverUser = this.userList[0];
    }
    var modalRef = this.modalService.open(this.modalConfirmation);
    modalRef.result.then((result) => {
      if (this.receiverUser) {
        let url = `${environment.endPoints.forwardEvent}${this.services.user.user}/${this.receiverUser}/${this.actionCardComment}`;
        this.services.update(url, events).subscribe(data => {
          this.childCardFeedComponent.closeDashboard();
        }, err => {
          console.error(err);
        });
      }
    }).catch((error) => {
      if (error) console.log(error);
    });
    this.eventsToForward = [];
  }

  mergeEvents(cards) {
    this.resetModalConfigurations();
    this.modalComment = true;
    this.modalTitle = "Mesclar cartões";
    var modalRef = this.modalService.open(this.modalConfirmation);
    modalRef.result.then((result) => {
      let url = `${environment.endPoints.mergeEvents}${this.services.user.user}/${this.actionCardComment}`;
      var eventsToMerge = cards.map(c => {
        return c.customObject;
      });
      this.services.update(url, eventsToMerge).subscribe(data => {
        // this.eventsToRemove = cards;
        cards.forEach(card => {
          let index = this.cards.findIndex(c => c.id == card.id);
          this.cards.splice(index, 1);
        });
        this.addOrUpdateCard(data[0]);
        console.log("Eventos mesclados!")
      }, err => {
        console.error(err);
      });
    }).catch((error) => {
      if (error) console.log(error);
    });
  }

  getUser() {
    //let user = this.services.user;
    let users;
    this.services.getAllFilter('Users', {}).subscribe(result => {
      users = result.dataList;
      //this.user = users.find(u => u.name == user.name);
    },
      err => {
        console.log('Erro ao obter os dados', err);
      });

    this.userFilters = this.services.user.filter;
  }

  applyFilters(filter) {
    this.services.setToken(this.services.user.user, filter);
  }

  verifyExpiredCards() {
    setInterval(() => {
      this.cards.forEach(c => {
        if (this.isExpiredCard(c)) {
          this.finishExpiredEvent(c);
        }
      })
    }, 50000);
  }

  finishExpiredEvent(card: Card) {
    let url = `${environment.endPoints.finishEvent}`;
    this.services.update(url, [card.customObject]).subscribe(data => {
      this.addOrUpdateCard(data);
    }, err => {
      console.error(err);
    });
  }

  isToRemoveFromFeed(lastEvent: any, lastEventForUser: any) {
    if (lastEvent.type == UserActionType.Forward && lastEvent.receiverUser == this.services.user.user) return false;

    if (lastEventForUser.type == UserActionType.Reject || lastEventForUser.type == UserActionType.Delete) {
      return true;
    }
    //Não remove do feed cartões finalizados
    if (lastEvent.type == UserActionType.Expire) {
      return true;
    }
    return false;
  }

  resetModalConfigurations() {
    this.modalTitle = null;
    this.modalComment = false;
    this.modalSelect = false;
    this.modalMessage = null;
    this.actionCardComment = "";
    this.receiverUser = null;
  }

  getMultiCardButtons() {
    if (this.services.userHasFeatureAccess("EventHypervision", "AcceptEvent", "edit")) {
      let e: CardAction = {
        disabled: false,
        hidden: false,
        type: ActionType.PUT,
        url: environment.baseUrl + environment.endPoints.acceptEvent,
        value: "Aceitar",
        btnClass: "btn btn-success btn-sm mr-1",
        callBack: (cards: Card[]) => {
          if (!this.checkAcceptedEvents(cards)) {
            this.activeCard = cards[0];
            var cardsToAccept = cards.map(c => {
              return c.customObject;
            })
            let url = `${environment.endPoints.acceptEvent}${this.services.user.user}`;
            this.services.update(url, cardsToAccept).subscribe(data => {
            }, err => {
              console.error(err);
            });
          }
        }
      };
      this.multiCardButtons.push(e);
    }

    if (this.services.userHasFeatureAccess("EventHypervision", "RejectEvent", "edit")) {
      let f: CardAction = {
        hidden: false,
        disabled: false,
        type: ActionType.PUT,
        url: environment.baseUrl + environment.endPoints.rejectEvent,
        value: "Rejeitar",
        btnClass: "btn btn-danger btn-sm mr-1",
        callBack: (cards: Card[]) => {
          this.resetModalConfigurations();
          if (!this.checkAcceptedEvents(cards)) {
            this.modalComment = true;
            this.modalTitle = "Rejeitar Cartões";
            var modalRef = this.modalService.open(this.modalConfirmation);
            modalRef.result.then((result) => {
              var cardsToReject = cards.map(c => {
                return c.customObject;
              });
              let url = `${environment.endPoints.rejectEvent}${this.services.user.user}/${this.actionCardComment}`;
              this.services.update(url, cardsToReject).subscribe(data => {
              }, err => {
                console.error(err);
              });
              this.childCardFeedComponent.closeDashboard();

            }).catch((error) => {
              if (error) console.log(error);
            });
          }
        }
      }
      this.multiCardButtons.push(f);
    }

    if (this.services.userHasFeatureAccess("EventHypervision", "DeleteEvent", "edit")) {
      let g: CardAction = {
        hidden: false,
        disabled: false,
        type: ActionType.PUT,
        url: environment.baseUrl + environment.endPoints.deleteEvent,
        value: "Remover",
        btnClass: "btn btn-secondary btn-sm mr-1",
        callBack: (cards: Card[]) => {
          this.resetModalConfigurations();
          if (!this.checkAcceptedEvents(cards)) {
            this.modalTitle = "Confirmação";
            this.modalMessage = "Remover Cartões?";
            this.modalRemove = this.modalService.open(this.modalConfirmation);
            this.eventsToRemove = cards;
          }
        }
      }
      this.multiCardButtons.push(g);
    }

    if (this.services.userHasFeatureAccess("EventHypervision", "ForwardEvent", "edit")) {
      let h: CardAction = {
        hidden: false,
        disabled: false,
        type: ActionType.PUT,
        url: environment.baseUrl + environment.endPoints.forwardEvent,
        value: "Encaminhar",
        btnClass: "btn btn-danger btn-sm mr-1",
        callBack: (cards: Card[]) => {
          this.services.getUsers((users) => {
            this.userList = UtilitiesService.subtractArrays(users, [this.services.user.user]);
            this.eventsToForward = cards;
            this.forwardEvents();
          })
        }
      }
      this.multiCardButtons.push(h);

    }

    // if (this.services.userHasFeatureAccess("EventHypervision", "MergeEvent", "edit")) {
    let i: CardAction = {
      disabled: false,
      hidden: false,
      type: ActionType.PUT,
      url: environment.baseUrl + environment.endPoints.mergeEvents,
      value: "Mesclar",
      btnClass: "btn btn-secondary btn-sm mr-1",
      callBack: (cards: Card[]) => {
        this.resetModalConfigurations();
        if (!this.checkAcceptedEvents(cards)) {
          this.mergeEvents(cards);
        }
      }
    }
    this.multiCardButtons.push(i);

  }

  userHasPermission(moduleName: string, featureName: string, permissionType: string) {

    if (this.services.userHasFeatureAccess(moduleName, featureName, permissionType)) {
      return true;
    }

    return false;
  }

  checkAcceptedEvents(cards) {
    this.acceptedEvent = cards.find(c => c.customObject.lastEvent.type === UserActionType.Accept);
    if (this.acceptedEvent) {
      this.resetModalConfigurations();
      this.modalTitle = "Eventos aceitos";
      this.modalMessage = "Existem eventos já aceitos na lista, remova-os para concluir a ação!";
      this.modalRemove = this.modalService.open(this.modalConfirmation);
      return true;
    } else {
      return false;
    }
  }

  getConfiguration() {
    // this.services.get(environment.endPoints.getConfiguration).subscribe(d => {
    //   if (d) {
    //     // this.eventFeedConfig.levels = this.getLevels(d.categoryCardConfigurations);
    //     if (d.eventFeedConfigurations) {
    //       this.eventFeedConfig = d.eventFeedConfigurations;
    //       this.eventFeedConfig.levels = this.getLevels(d.categoryCardConfigurations);
    //       this.eventFeedConfig.descendingOrder = true;
    //       this.eventFeedConfig.orderBySeverity = true;
    //       this.eventFeedConfig.orderByProperty = "dateStart";
    //     }
    //     else
    this.eventFeedConfig = this.eventFeedConfigDefault;

    // this.timelineConfig = d.timelineConfigurations ? d.timelineConfigurations : this.timelineConfigDefault;
    // }
    // else {
    //   // this.levels = this.levelsDefault;
    //   this.eventFeedConfig = this.eventFeedConfigDefault;
    // }
    this.timelineConfig = this.timelineConfigDefault;
    // }, err => {
    //   console.error(err);
    // });
  }

  getLevels(categoryLevels) {
    var levels: any;
    levels = Object.values(categoryLevels);

    this.levels = levels.map(item => {
      return {
        name: item.name ? item.name : this.severityTypes.find(m => m.type == item.category).value,
        color: item.color,
        category: item.category,
        type: this.severityTypes.find(m => m.type == item.category).value,
        notificationSound: item.notificationSound,
        repeatNotificationSound: item.repeatNotificationSound
      }
    });

    return this.levels;
  }

  search = (text$: Observable<string>) => {
    const users = this.userList;
    const debouncedText$ = text$.pipe(debounceTime(10), distinctUntilChanged());
    const clicksWithClosedPopup$ = this.click$.pipe(filter(() => this.instance && !this.instance.isPopupOpen()));
    const inputFocus$ = this.focus$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? users
        : users.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }

  validateComment() {
    return this.actionCardComment.match('^([a-zA-Z\u00C0-\u017F´\\d_\\-])[a-zA-Z\u00C0-\u017F´\\s_\\-\\d\\D]{2,}$');
  }
}
