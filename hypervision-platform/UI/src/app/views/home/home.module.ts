import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home.routing.module';
import { ConcertCardfeedIoModule } from '@xst/concert-cardfeed-io';
import { ConcertTimelineIoModule } from '@xst/concert-timeline-io';
import { ConcertNavbarIoModule } from '@xst/concert-navbar-io';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HomeRoutingModule,
    ConcertCardfeedIoModule,
    ConcertTimelineIoModule,
    ConcertNavbarIoModule,
    NgbModule
  ]
})
export class HomeModule { }
