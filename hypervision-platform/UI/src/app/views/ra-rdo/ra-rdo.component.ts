import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { environment } from 'src/environments/environment';
import { ConcertLoadingIoService } from '@xst/concert-loading-io';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EventType } from 'src/app/models/EventType';
import { ActionType } from '@xst/concert-cardfeed-io';
import { UserActionType } from 'src/app/models/Cards.model';

@Component({
  selector: 'app-ra-rdo',
  templateUrl: './ra-rdo.component.html',
  styleUrls: ['./ra-rdo.component.scss']
})
export class RARdoComponent implements OnInit {
  eventId: string;
  events: any;
  lastEvent: any;
  cycle: string;
  status: string;
  //lastEventsByUser: any;
  //lastEventUser: any;

  comment: string = "";
  modalTitle: string = "";
  modalButton: string = "";
  currentUser: string = "";

  @ViewChild('modal') private modalReject: TemplateRef<any>;

  constructor(
    private services: CommonService,
    private route: ActivatedRoute,
    private loadingService: ConcertLoadingIoService,
    private modalService: NgbModal) { }

  ngOnInit(): void {
    this.services.checkIfIsLogged();
    this.currentUser = this.services.user.user;
    this.eventId = this.route.snapshot.paramMap.get('eventId');
    if (this.services.userHasFeatureAccess("EventHypervision", "GetRDO", "view")) {
      this.getEvents();
    }
  }

  getEvents() {
    this.loadingService.show();
    let url = `${environment.endPoints.getRDO}3/${this.eventId}`;
    this.services.get(url).subscribe(data => {
      this.events = data;
      // if(data[data.length -1].lastEvent.type == UserActionType.Forward){
      //   this.events = data;
      // }else{
      //   this.events = data.filter(e => {
      //     return this.services.user.user == e.lastEvent.user || e.lastEvent.user == null;
      //   });
      // }
      this.lastEvent = this.events[this.events.length - 1];
      this.setStatus(event);
      this.loadingService.hide();
    }, err => {
      this.loadingService.hide();
      console.error(err);
    });

  }

  setStatus(event) {
    var fullStatus = this.lastEvent.cardDetails.status;
    if (fullStatus.endsWith("NS")) {
      this.status = "Não satisfatório";
      this.cycle = fullStatus.substring(0, fullStatus.length - 2);
    } else if (fullStatus.endsWith("S")) {
      this.status = "Satisfatório";
      this.cycle = fullStatus.substring(0, fullStatus.length - 1);
    } else {
      this.status = "Em andamento";
      this.cycle = fullStatus;
    }
  }

  startEvent() {
    this.loadingService.show();
    let url = `${environment.endPoints.addActionOnEvent}7/${this.services.user.user}/Iniciou a ocorrência`;
    this.services.update(url, [this.lastEvent]).subscribe(data => {
      this.loadingService.hide();
      this.getEvents();
    }, err => {
      console.error(err);
    });
  }

  addComment() {
    this.modalTitle = "Incluir ação";
    this.modalButton = "Incluir";
    var modalRef = this.modalService.open(this.modalReject);
    modalRef.result.then((result) => {
      this.loadingService.show();
      let url = `${environment.endPoints.addActionOnEvent}4/${this.services.user.user}/${this.comment}`;
      this.services.update(url, [this.lastEvent]).subscribe(data => {
        this.loadingService.hide();
        this.getEvents();
        this.comment = "";
      }, err => {
        console.error(err);
      });
    }, err => {
      console.error(err);
    });

  }

  finishEvent() {
    this.modalTitle = "Encerrar ocorrência";
    this.modalButton = "Finalizar";
    var modalRef = this.modalService.open(this.modalReject);
    modalRef.result.then((result) => {
      this.loadingService.show();
      let url = `${environment.endPoints.addActionOnEvent}5/${this.services.user.user}/${this.comment}`;
      this.services.update(url, [this.lastEvent]).subscribe(data => {
        this.loadingService.hide();
        this.getEvents();
        this.comment = "";
      }, err => {
        console.error(err);
      });
    }, err => {
      console.error(err);
    });
  }

  userHasPermission(moduleName: string, featureName: string, permissionType: string) {

    if (this.services.userHasFeatureAccess(moduleName, featureName, permissionType)) {
      return true;
    }

    return false;
  }

  validateComment() {
    return this.comment.match('^([a-zA-Z\u00C0-\u017F´\\d_\\-])[a-zA-Z\u00C0-\u017F´\\s_\\-\\d\\D]{2,}$') ? false : true;
  }

  showButton(type: string): boolean {
    if (this.lastEvent.lastEvent.type == UserActionType.Forward || this.lastEvent.lastEvent.type == UserActionType.Finish) return false;

    switch (type) {
      case 'startEvent': return this.lastEvent.lastEvent.user == this.currentUser && this.lastEvent.lastEvent.type == UserActionType.Accept;
      case 'addAction':
      case 'finishEvent': return this.lastEvent.lastEvent.user == this.currentUser && this.lastEvent.lastEvent.type > UserActionType.Accept;
    }

  }

}
