import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RARdoComponent } from './ra-rdo.component';

const routes: Routes = [
    {
        path: '',
        component: RARdoComponent,
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RARDORoutingModule {}
