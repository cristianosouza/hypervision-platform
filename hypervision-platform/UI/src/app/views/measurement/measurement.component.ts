import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { environment } from 'src/environments/environment';
import { ConcertLoadingIoService } from '@xst/concert-loading-io';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserActionType } from 'src/app/models/Cards.model';
import { Chart } from 'angular-highcharts';

@Component({
  selector: 'app-measurement',
  templateUrl: './measurement.component.html',
  styleUrls: ['./measurement.component.scss']
})
export class MeasurementComponent implements OnInit {
  eventId: string;
  events: any;
  lastEvent: any;
  lastEventsByUser: any;
  lastEventUser: any;
  chart: Chart;
  series: any[] = [];
  historicalColor: string = "#FC1D1D";
  predictedColor: string = '#1DDD43';
  currentDatetime: any;

  comment: string = "";
  modalTitle: string = "";
  modalButton: string = "";
  currentUser: string = "";

  @ViewChild('modal') private modalReject: TemplateRef<any>;

  constructor(
    private services: CommonService,
    private route: ActivatedRoute,
    private loadingService: ConcertLoadingIoService,
    private modalService: NgbModal) { }

  ngOnInit(): void {
    this.services.checkIfIsLogged();
    this.currentUser = this.services.user.user;
    this.eventId = this.route.snapshot.paramMap.get('eventId');
    if (this.services.userHasFeatureAccess("EventHypervision", "GetRDO", "view")) {
      this.getEvents();
    }

  }

  getEvents() {
    this.loadingService.show();
    let url = `${environment.endPoints.getRDO}2/${this.eventId}`;
    this.services.get(url).subscribe(data => {
      this.events = data;

      this.lastEvent = this.events[this.events.length - 1];
      
      this.series.push(this.getSeries(this.lastEvent.cardDetails.historicalXPlot, this.lastEvent.cardDetails.historicalYPlot, "Dados históricos", this.historicalColor));
      this.series.push(this.getSeries(this.lastEvent.cardDetails.predictedXPlot, this.lastEvent.cardDetails.predictedYPlot, "Dados previstos", this.predictedColor));
      this.currentDatetime = (new Date(this.lastEvent.cardDetails.currentDatetime).getTime() - (3 * 3600000));

      this.chartMeasurement();
      this.loadingService.hide();
    }, err => {
      this.loadingService.hide();
      console.error(err);
    });

  }

  chartMeasurement() {
    this.chart = new Chart({
      lang: {
        months: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        shortMonths: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        weekdays: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
      },
      chart: {
        type: 'spline',
        backgroundColor: "#555e63",
        scrollablePlotArea: {
          minWidth: 600,
          scrollPositionX: 1
        },
        style: {
          color:"#ffffff"
        }
      },
      title: {
        text: 'Medição suspeita de alarmes',
        align: 'left',
        style: {
          color:"#ffffff"
        }
      },
      subtitle: {
        text: 'Gráfico das medições passadas e previstas',
        align: 'left',
        style: {
          color:"#ffffff"
        }
      },
      xAxis: {
        type: 'datetime',
        min: (this.currentDatetime - (24 * 3600000)),
        max: (this.currentDatetime + (24 * 3600000)),
        labels: {
          overflow: 'justify',
          style: {
            color:"#ffffff"
          }
        },
        plotLines: [{
          color: "#ffffff",
          width: 3,
          value: this.currentDatetime
        }]
      },
      yAxis: {
        title: {
          text: 'Valor alarme',
          style: {
            color:"#ffffff"
          }
        },
        labels: {
          style: {
            color:"#ffffff"
          }
        },
        minorGridLineWidth: 0,
        gridLineWidth: 0,
        alternateGridColor: null,
        plotBands: []
      },
      tooltip: {
        valueSuffix: ''
      },
      plotOptions: {
        spline: {
          lineWidth: 4,
          states: {
            hover: {
              lineWidth: 5
            }
          },
          marker: {
            enabled: false
          }
        }
      },
      series: this.series
    });
  }

  approveMeasurement() {
    this.loadingService.show();
    let url = `${environment.endPoints.addActionOnEvent}11/${this.services.user.user}/Aprovou a medição`;
    debugger
    this.services.update(url, [this.lastEvent]).subscribe(data => {
      this.loadingService.hide();
      this.finishEvent();
      this.getEvents();
    }, err => {
      console.error(err);
    });
  }

  disapproveMeasurement() {
    this.modalTitle = "Desaprovar medição";
    this.modalButton = "Confirmar";
    var modalRef = this.modalService.open(this.modalReject);
    modalRef.result.then((result) => {
      this.loadingService.show();
      let url = `${environment.endPoints.addActionOnEvent}12/${this.services.user.user}/${this.comment}`;
      this.services.update(url, [this.lastEvent]).subscribe(data => {
        this.loadingService.hide();
        this.finishEvent();
        this.getEvents();
        this.comment = "";
      }, err => {
        console.error(err);
      });
    }, err => {
      console.error(err);
    });
  }

  finishEvent() {
    let url = `${environment.endPoints.addActionOnEvent}5/${this.services.user.user}/Evento encerrado`;
    this.services.update(url, [this.lastEvent]).subscribe(data => {
      return
    }, err => {
      console.error(err);
      return
    });
  }

  getSeries(xPlot, yPlot, name, color) {
    let i = 0;
    let data: any[] = [];
    var dateTime = 0;
    if (xPlot) {
      for (i; i < xPlot.length; i++) {
        dateTime = (xPlot[i] - (3 * 3600000));
        data.push([dateTime, yPlot[i]]);
      }
    }
    return {
      type: undefined,
      name: name,
      color: color,
      data: data,
      lineWidth: 1.5
    }
  }

  userHasPermission(moduleName: string, featureName: string, permissionType: string) {

    if (this.services.userHasFeatureAccess(moduleName, featureName, permissionType)) {
      return true;
    }

    return false;
  }

  validateComment() {
    return this.comment.match('^([a-zA-Z\u00C0-\u017F´\\d_\\-])[a-zA-Z\u00C0-\u017F´\\s_\\-\\d\\D]{2,}$');
  }

  showButton(type: string): boolean {
    if (this.lastEvent.lastEvent.type == UserActionType.Forward || this.lastEvent.lastEvent.type == UserActionType.Finish) return false;

    switch (type) {
      case 'approveMeasurement': return this.lastEvent.lastEvent.user == this.currentUser && this.lastEvent.lastEvent.type == UserActionType.Accept;
      case 'disapproveMeasurement': return this.lastEvent.lastEvent.user == this.currentUser && this.lastEvent.lastEvent.type == UserActionType.Accept;
    }

  }
}