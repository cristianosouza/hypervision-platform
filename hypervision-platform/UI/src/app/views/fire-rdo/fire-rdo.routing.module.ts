import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FireRdoComponent } from './fire-rdo.component';

const routes: Routes = [
    {
        path: '',
        component: FireRdoComponent,
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FireRDORoutingModule {}
