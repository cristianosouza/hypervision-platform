import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaultRdoComponent } from './fault-rdo.component';
import { FaultRdoRoutingModule } from './fault-rdo.routing.module';
import { ConcertLoadingIoModule } from '@xst/concert-loading-io';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    FaultRdoComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    FaultRdoRoutingModule,
    ConcertLoadingIoModule
  ]
})
export class FaultRDOModule { }
