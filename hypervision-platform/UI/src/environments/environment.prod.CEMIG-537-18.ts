export const environment = {
  production: true,
  hubUrl: "/eventHub",
  baseUrl: "/",
  authentication: "http://10.230.5.144:5001/",
  endPoints: {
    getEvents: "event/GetEvents/",
    acceptEvent: "event/AcceptEvent/",
    rejectEvent: "event/RejectEvent/",
    finishEvent: "event/FinishExpiredEvent/",
    deleteEvent: "event/DeleteEvent/",
    forwardEvent: "event/ForwardEvent/",
    registerUser: "event/RegisterUser/",
    mergeEvents: "event/MergeEvents/",
    getFields: "configuration/GetFields/",
    getUrls: "configuration/GetUrls/",
    getActiveUsers: "configuration/GetActiveUsers/",
    getConfiguration: "configuration/GetConfiguration/",
    saveConfiguration: "configuration/Save/",
    uploadFile: "configuration/OnPostUploadAsync/",
    getRDO: "event/GetRDO/",
    addActionOnEvent: "event/AddActionOnEvent/"
  }
};
