﻿using Commons_Core.DAO;
using Commons_Core.Services.LogService;
using hypervision_platform.Models;
using hypervision_platform.Models.Enum;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace hypervision_platform.DAO
{
    public class ConfigurationDAO : MongoBaseDAO<Configuration>
    {

        public ConfigurationDAO(MongoContext context) : base(context) { }

        private static readonly string _path = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "config");
        public Configuration GetConfiguration()
        {
            var configuration = FindOne(_ => true);

            if (configuration == null)
            {
                LogServiceConnection.Debug<string>($"Uma configuração padrão foi criada");
                configuration = GetDefaultConfiguration();
                InsertOne(configuration);
                return FindOne(_ => true);
            }

            return configuration;
        }

        private Configuration GetDefaultConfiguration()
        {
            var config = new Configuration();
            var categoryCardConfigurations = new Dictionary<string, CategoryCardConfiguration>
            {
                {
                    Severity.INFO.ToString(),
                    new CategoryCardConfiguration()
                    {
                        Color = "#00ff32",
                        NotificationSound = "../../../assets/audio/sirene.mp3",
                        Category = Severity.INFO,
                        RepeatNotificationSound = false
                    }
                },//Sucesso - VERDE
                {
                    Severity.NOTIFICATION.ToString(),
                    new CategoryCardConfiguration()
                    {
                        Color = "#359afa", NotificationSound = "../../../assets/audio/sirene.mp3",
                        Category = Severity.NOTIFICATION,
                        RepeatNotificationSound = false
                    }
                },//Notificação - AZUL
                {
                    Severity.ALERT.ToString(),
                    new CategoryCardConfiguration()
                    {
                        Color = "#ffc107",
                        NotificationSound = "../../../assets/audio/sirene.mp3",
                        Category = Severity.ALERT,
                        RepeatNotificationSound = false
                    }
                }, //Alerta - AMARELO                
                {
                    Severity.ALARM.ToString(),
                    new CategoryCardConfiguration()
                    {
                        Color = "#c0392b",
                        NotificationSound = "../../../assets/audio/alarme.mp3",
                        Category = Severity.ALARM,
                        RepeatNotificationSound = false
                    }
                }//Alarme - VERMELHO
            };
            config.Update(LogLevel.Error, null, categoryCardConfigurations, null, null, null);

            return config;
        }
    }
}

