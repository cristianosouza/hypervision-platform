using Commons_Core.Globals;
using Commons_Core.Services.LogService;
using hypervision_platform.Business.Interfaces;
using hypervision_platform.Services;
using hypervision_platform.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;

namespace hypervision_platform
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            var kafkaServer = Environment.GetEnvironmentVariable("KAFKA_SERVER");
            var schemaServer = Environment.GetEnvironmentVariable("SCHEMA_REGISTRY_URL");
            var productName = Globals.Configuration.GetSection("Product").Value;
            var applicationName = Globals.Configuration.GetSection("ApplicationName").Value;
            LogServiceConnection.SetConfigurations(productName, applicationName, kafkaServer, schemaServer, LogLevel.Error);
            LogServiceConnection.Info<string>($"{applicationName} foi conectada ao tópico de log com sucesso!");

            new AuthenticationControl().RegisterModulesAndFeaturesOnAuthenticationService();
        }

        //public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // services.AddCors(options =>
            // {
            //     options.AddPolicy("CorsPolicy",
            //         builder => builder.WithOrigins("http://localhost:4200")
            //             .AllowAnyMethod()
            //             .AllowAnyHeader()
            //             .AllowCredentials());
            // });
            services.AddCors();

            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "UI/dist";
            });
            services.AddSwaggerGen();
            services.AddSignalR();
            services.AddControllers();
            services.AddSingleton<IUserIdProvider, NameUserIdProvider>();
            services.AddScoped<IConfigurationBusiness, ConfigurationBusiness>();
            services.AddScoped<IConfigurationService, ConfigurationService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //sapp.UseHttpsRedirection();            
            app.UseRouting();
            app.UseCors(builder =>
             {
                 builder.SetIsOriginAllowed(s => true);
                 //builder.AllowAnyOrigin();
                 builder.AllowAnyMethod();
                 builder.AllowAnyHeader();
                 builder.AllowCredentials();
             });
            // app.UseAuthorization();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Hypervision Platform API");
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<EventHub>("/eventhub");
            });

            app.UseStaticFiles();
            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();
            }
            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501
                spa.Options.SourcePath = "UI";
                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }
    }
}
