﻿namespace hypervision_platform.Models.Enum
{
    /// <summary>
    /// Na ordem crescente de severidade
    /// </summary>
    public enum Severity
    {
        INFO,
        NOTIFICATION,
        ALERT,
        ALARM
    }
}