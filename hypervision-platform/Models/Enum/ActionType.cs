﻿namespace hypervision_platform.Models
{
    public enum ActionType
    {
        None,
        Accept,
        Reject,
        Share,
        Comment,
        Finish,
        Expire,
        Start,
        Delete,
        Forward,
        Merge,
        Approve,
        Disapprove
    }
}