﻿using System;
using System.Collections.Generic;

namespace hypervision_platform.Models
{
    public class CardEvent
    {
        public string User { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public ActionType Type { get; set; }
        public string Comment { get; set; }
        public List<string> Regions { get; set; }
        public string ReceiverUser { get; set; }
        public List<string> MergedCardsIds { get; set; }

        public static string GetActionDescription(ActionType actionType)
        {
            switch (actionType)
            {
                case ActionType.Accept:
                    return "Aceitou cartão";
                case ActionType.Reject:
                    return "Rejeitou cartão";
                case ActionType.Expire:
                    return "Cartão expirou";
                case ActionType.Delete:
                    return "Removeu cartão";
                case ActionType.Forward:
                    return "Encaminhou cartão";
                case ActionType.Merge:
                    return "Mesclou cartões";
                case ActionType.Finish:
                    return "Finalizou o cartão";
                default:
                    return "Realizou ação no cartão";
            }
        }

    }
}
