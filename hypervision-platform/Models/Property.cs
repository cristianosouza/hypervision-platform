namespace hypervision_platform.Models
{
    public class Property
    {
        public string Name { get; set; }
        public string Label { get; set; }
        public string Value { get; set; }
        public string Field { get; set; }
        public PropertyType Type { get; set; }
    }
}