using Commons_Core.Entities;

namespace hypervision_platform.Models
{
    public class TimelineConfiguration : Document
    {
        public string CurrentTimeColor { get; set; }
        public string GraphBackgroundColor { get; set; }
        public string ResumeBackgroundColor { get; set; }
        public string FontColor { get; set; }
        public int FontSize { get; set; }
        public string FontFamily { get; set; }
        public int Heigth { get; set; }
        public int InitialLimit { get; set; }
        public int FinalLimit { get; set; }
    }

}
