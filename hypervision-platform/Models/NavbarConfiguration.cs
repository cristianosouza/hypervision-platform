using Commons_Core.Entities;

namespace hypervision_platform.Models
{
    public class NavbarConfiguration : Document
    {
        public string BackgroundColor { get; set; }
        public bool ShowNotification { get; set; }
        public MainLogo MainLogo { get; set; }
        public Filters Filters { get; set; }
    }

    public class MainLogo
    {
        public string Image { get; set; }
        public string Name { get; set; }
        public int FontSize { get; set; }
    }

    public class Filters
    {
        public bool Multiple { get; set; }
        public string Name { get; set; }
    }

}


