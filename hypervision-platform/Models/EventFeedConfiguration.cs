using Commons_Core.Entities;

namespace hypervision_platform.Models
{
    public class EventFeedConfiguration : Document
    {
        public string FeedBackgroundColor { get; set; }
        public string CardBackgroundColor { get; set; }
        public string DashboardBackgroundColor { get; set; }
        public string FontColor { get; set; }
        public string FeedWidth { get; set; }
        public CategoryCardConfiguration[] Levels { get; set; }
        public bool DisplayDateTime { get; set; }
        public bool DisplayFilter { get; set; }
        public bool AllowMultiselectionCard { get; set; }
        public bool SoundAlert { get; set; }
        public bool AllowHideCard { get; set; }
        public bool OrderBySeverity { get; set; }
    }
}
