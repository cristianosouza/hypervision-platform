﻿using System;
using System.Collections.Generic;

namespace hypervision_platform.Models
{
    public class CardDetails
    {
        public List<string>? Regions { get; set; }
        public List<string>? SubRegions { get; set; }
        public List<string>? Lines { get; set; }
        public List<string>? Substations { get; set; }
        public List<string>? Cities { get; set; }
        public List<double>? Poligon { get; set; }
        public string? Latitude { get; internal set; }
        public string? Longitude { get; internal set; }
        public List<string> Structures { get; internal set; }
        public string Nature { get; internal set; }
        public string Status { get; set; }

        // TODO: remover propriedades não essenciais para exibição no cartão e mover as demais para hash map
        public double AlarmValue { get; set; }
        public double PredictedAlarm { get; set; }
        public double DifferenceAlarm { get; set; }
        public List<long> HistoricalXPlot { get; set; }
        public List<double> HistoricalYPlot { get; set; }
        public List<long> PredictedXPlot { get; set; }
        public List<double> PredictedYPlot { get; set; }
        public long EventId { get; set; }
        public long ObjId { get; set; }
        public DateTime? OriginDatetime { get; set; }
        public DateTime? OriginNormDatetime { get; set; }
        public DateTime CurrentDatetime { get; set; }
        public DateTime RepositoryDatetime { get; set; }
        public DateTime? RepositoryNormDatetime { get; set; }
        public string Tag { get; set; }
        public string TagRem { get; set; }
        public string AreaSegEnt { get; set; }
        public string Synoptic { get; set; }
        public string Group { get; set; }
        public string Class { get; set; }
        public string Description { get; set; }
        public int Priority { get; set; }
        public string AlarmStatus { get; set; }
        public string NormalizeStatus { get; set; }
        public double? NormalizeValue { get; set; }
        public long? IDPTOVINC { get; set; }
        public int Origin { get; set; }
        public int Normalize { get; set; }
        public int AlarmRange { get; set; }
        public int? NormalizeRange { get; set; }
        public int Blink { get; set; }
        public int EntityType { get; set; }
        public int Type { get; set; }

        public List<List<Property>> Properties { get; set; }
    }
}