﻿using hypervision_platform.Models;
using hypervision_platform.Models.Enum;
using System;
using System.Collections.Generic;

namespace hypervision_platform.DVO
{
    public class CardVO
    {
        public string Id { get; set; }
        public string Level { get; set; }
        public bool Dispached { get; set; }
        public Severity Severity { get; set; }
        public DateTime Date { get; set; }
        public DateTime EventStart { get; set; }
        public DateTime? EventEnd { get; set; }
        public CardEvent LastEvent { get; set; }
        public List<CardEvent> EventHistory {get;set;}
        public EventType EventType { get; set; }
        public CardDetails CardDetails { get; set; }
        public string MergedCardId { get; set; }        
    }
}
