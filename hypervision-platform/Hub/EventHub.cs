using Commons_Core.Services.LogService;
using hypervision_platform.DAO;
using hypervision_platform.DVO;
using hypervision_platform.Models;
using hypervision_platform.Models.Enum;
using Microsoft.AspNetCore.SignalR;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace hypervision_platform
{
    public class NameUserIdProvider : IUserIdProvider
    {
        public string? GetUserId(HubConnectionContext connection)
        {
            return connection?.User?.Identity?.Name;
        }
        public class EmailBasedUserIdProvider : IUserIdProvider
        {
            public virtual string? GetUserId(HubConnectionContext connection)
            {
                return connection?.User?.FindFirst(ClaimTypes.Email)?.Value;
            }
        }
    }
    public class EventHub : Hub, IDisposable
    {
        private static Dictionary<string, string> _clients = new Dictionary<string, string>();
        internal static Dictionary<string, HashSet<string>> RegisteredUsers { get; set; }
        private IHubCallerClients _Clients;
        private MetereologicalEventBusiness _meteorologicalEvents { get; set; }
        private FireEventBusiness _fireEvents { get; set; }
        private TagMeasurementEventBusiness _tagMeasurementEvents { get; set; }
        private RAEventBusiness _RAEvents { get; set; }

        private FaultEventBusiness _FaultEvents { get; set; }
        private readonly RdoDAO _rdoDAO;

        public EventHub()
        {
            _rdoDAO = new RdoDAO(new MongoContext());

            if (RegisteredUsers == null)
            {
                RegisteredUsers = new Dictionary<string, HashSet<string>>();
            }
            InitializeFireEvents();
            InitializeMeteorologicalEvents();
            InitializeTagMeasurementEvents();
            InitializeRAEvents();
            InitializeFaultEvents();
        }

        private void InitializeMeteorologicalEvents()
        {
            _meteorologicalEvents = new MetereologicalEventBusiness(_rdoDAO);
            _meteorologicalEvents.CardReceivedEvent += OnData;
            _meteorologicalEvents.StartTask();
            LogServiceConnection.Debug<string>("Eventos meteorologicos iniciados.");
        }

        private void InitializeFireEvents()
        {
            _fireEvents = new FireEventBusiness(_rdoDAO);
            _fireEvents.CardReceivedEvent += OnData;
            _fireEvents.StartTask();
            LogServiceConnection.Debug<string>("Eventos de queimada iniciados.");
        }

        private void InitializeTagMeasurementEvents()
        {
            _tagMeasurementEvents = new TagMeasurementEventBusiness(_rdoDAO);
            _tagMeasurementEvents.CardReceivedEvent += OnData;
            _tagMeasurementEvents.StartTask();
            LogServiceConnection.Debug<string>("Eventos de alertas de medição iniciados.");
        }

        private void InitializeRAEvents()
        {
            _RAEvents = new RAEventBusiness(_rdoDAO);
            _RAEvents.CardReceivedEvent += OnData;
            _RAEvents.StartTask();
            LogServiceConnection.Debug<string>("Eventos de RA iniciados.");
        }

        private void InitializeFaultEvents()
        {
            _FaultEvents = new FaultEventBusiness(_rdoDAO);
            _FaultEvents.CardReceivedEvent += OnData;
            _FaultEvents.StartTask();
            LogServiceConnection.Debug<string>("Eventos de Falta iniciados.");
        }

        private async Task OnData(Card card)
        {
            try
            {
                var cardVO = new CardVO
                {
                    Id = card.CardId,
                    Level = card.Level,
                    Severity = card.Severity,
                    Date = card.Date,
                    EventStart = card.EventStart,
                    EventEnd = card.EventEnd,
                    LastEvent = card.Event,
                    EventType = card.EventType,
                    CardDetails = card.CardDetails,
                    Dispached = card.Dispached
                };
                if (card.EventEnd != null && card.EventEnd.Value.ToUniversalTime() < DateTime.Now.ToUniversalTime())
                {
                    SetExpiredCards(new List<CardVO> { cardVO });
                    return;
                }
                if (card != null && RegisteredUsers.Count > 0)
                {
                    foreach (var i in RegisteredUsers)
                    {
                        if (card.CardDetails.Regions?.Count > 0)
                        {
                            var filter = i.Value.ToList();
                            var exist = card.CardDetails.Regions.Select(r => r.Trim().ToUpper()).Intersect(filter).Count() > 0;
                            if (exist)
                            {
                                await _Clients.Group(i.Key).SendAsync("ReceiveMessage", cardVO);
                            }
                        }
                        else
                        {
                            await _Clients.All.SendAsync("ReceiveMessage", cardVO);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao tentar enviar o cartão: {ex.Message}");
            }
        }

        public override async Task OnConnectedAsync()
        {
            if (!_clients.ContainsKey(Context.ConnectionId))
            {
                _clients.Add(Context.ConnectionId, "");
            }
            _Clients = Clients;
            await base.OnConnectedAsync();
            LogServiceConnection.Debug<string>("Hub conectado.");
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            var name = _clients[Context.ConnectionId];
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, name);
            _clients.Remove(Context.ConnectionId);
            _Clients = Clients;
            await base.OnDisconnectedAsync(exception);
            UnregisterClient(name);
            LogServiceConnection.Debug<string>("Hub desconectado.");
        }

        public static async Task SendMessageToUser<T>(IHubContext<EventHub> context, string? user, T message)
        {
            if (string.IsNullOrEmpty(user))
            {
                await SendMessageToAll(context, message);
            }
            else
            {
                await context.Clients.Group(user).SendAsync("ReceiveMessage", message);
            }
            LogServiceConnection.Debug<string>("Mensagem enviada aos clientes.");
        }

        public static async Task SendMessageToAll<T>(IHubContext<EventHub> context, T message, string messageTag = "ReceiveMessage")
        {
            await context.Clients.All.SendAsync(messageTag, message);
        }

        public async Task RegisterClient(string user, string _filter)
        {
            var connectionId = Context.ConnectionId;
            _clients[connectionId] = user;
            RegisteredUsers[user] = new HashSet<string>();
            foreach (string f in _filter.Split(","))
            {
                RegisteredUsers[user].Add(f);
            }

            await Groups.AddToGroupAsync(connectionId, user);
            _Clients = Clients;
            //await Clients.All.SendAsync("ReceiveMessage", "Registred");
            LogServiceConnection.Debug<string>("Cliente registrado.");
        }

        public void UnregisterClient(string user)
        {
            RegisteredUsers.Remove(user);
            LogServiceConnection.Debug<string>("Registro do cliente cancelado.");
        }


        public async Task GetEvents(string user)
        {
            try
            {
                var cardsToSend = new List<CardVO>();
                var cards = _rdoDAO.FindAll()
                .Select(Converters.CardEntityToVO)
                .GroupBy(_ => _.Id)
                .Select(g => new { g.Key, Cards = g.ToList() })
                .ToList();

                foreach (var c in cards)
                {
                    //Verifica e finaliza cartões expirados
                    SetExpiredCards(c.Cards);

                    //Recupera o ultimo cartão
                    var lastCard = c.Cards.Last();

                    //Agrupa eventos por usuários
                    var eventsByUser = c.Cards
                    .Select(_ => _.LastEvent)
                    .GroupBy(_ => _.User)
                    .Select(g => new { g.Key, events = g.ToList() });

                    //Filtra para somente o último evento 
                    var lastEvents = new List<CardEvent>();
                    foreach (var e in eventsByUser)
                    {
                        var last = e.events.OrderBy(_ => _.Date).Last();
                        lastEvents.Add(last);
                    }
                    lastCard.EventHistory = lastEvents;


                    //Filtra eventos que foram removidos/rejeitados pelo usuário
                    var isToRemovedOrRejected = lastEvents.Any(e => e.User == user && (e.Type == ActionType.Delete || e.Type == ActionType.Reject));
                    //Filtra somente cartões expirados
                    var isExpired = lastCard.LastEvent.Type == ActionType.Expire;

                    var IsRDOForwarded = lastCard.LastEvent.Type == ActionType.Forward && lastCard.LastEvent.ReceiverUser == user;

                    if (c.Cards.Any(_ => _.LastEvent.Type == ActionType.Finish))
                    {
                        lastCard.LastEvent = c.Cards.FirstOrDefault(_ => _.LastEvent.Type == ActionType.Finish).LastEvent;
                    }

                    if ((!isToRemovedOrRejected || IsRDOForwarded) && !isExpired)
                    {
                        cardsToSend.Add(lastCard);
                    }
                }
                LogServiceConnection.Debug<string>($"Recuperou os eventos");
                cardsToSend.ForEach(async e =>
                {
                    await Clients.Group(user).SendAsync("ReceiveMessage", e);
                });
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao tentar buscar cartões: {ex.Message}");
            }
        }

        private void SetExpiredCards(List<CardVO> cards)
        {
            var card = cards.OrderBy(c => c.LastEvent.Date).Last();
            if (card.Severity == Severity.NOTIFICATION)
            {
                if (cards.Any(c => c.LastEvent.Type == ActionType.Expire)) return;

                if ((card.EventEnd != null && card.EventEnd.Value.ToUniversalTime() < DateTime.Now.ToUniversalTime()))
                {
                    switch (card.EventType)
                    {
                        case EventType.Meteorological:
                            var result = _meteorologicalEvents.AddAction(ActionType.Expire, card.Id, "");
                            LogServiceConnection.Info<string>("O cartão expirou e foi finalizado pelo sistema.");
                            cards.Add(result);
                            break;
                            //Por hora somente eventos de notificação tem prazo de validade.
                            // case EventType.TagMeasurement:
                            //     _tagMeasurementEvents.AddAction(ActionType.Expire, card.Id, "");
                            //     break;
                    }
                }
            }
            cards = cards.OrderBy(c => c.LastEvent.Date).ToList();
        }

        //private List<CardVO> FilterCardsByUserArea(List<CardVO> cards, string user)
        //{
        //    if (RegisteredUsers.ContainsKey(user))
        //    {
        //        List<string> filter = RegisteredUsers[user].Select(r => r.Trim().ToUpper()).ToList();
        //        var result = cards.Where(c => c.CardDetails.Regions.Select(r => r.Trim().ToUpper()).Intersect(filter).Count() > 0).ToList();
        //        LogServiceConnection.Debug<string>($"Cartões filtrados por área de atuação.");
        //        return result;
        //    }
        //    return cards;
        //}

        //private List<CardVO> FilterCardsByUserDelete(List<CardVO> cards, string user)
        //{
        //    var result = new List<CardVO>();

        //    foreach (var card in cards)
        //    {
        //        bool isDeleted = false;
        //        // FIXME: melhorar uso das instâncias por tipo de cartão
        //        switch (card.EventType)
        //        {
        //            case EventType.Fire:
        //                isDeleted = _fireEvents.IsRDODeletedByUser(card, user);
        //                break;
        //            case EventType.Meteorological:
        //                isDeleted = _meteorologicalEvents.IsRDODeletedByUser(card, user);
        //                break;
        //            case EventType.TagMeasurement:
        //                isDeleted = _tagMeasurementEvents.IsRDODeletedByUser(card, user);
        //                break;
        //        }
        //        if (!isDeleted)
        //        {
        //            result.Add(card);
        //        }
        //    }
        //    LogServiceConnection.Debug<string>($"Cartões apagados pelo usuário {user} filtrados.");
        //    return result;
        //}

        //private List<CardVO> FilterRejectedCardsByUserFields(List<CardVO> cards, string user)
        //{
        //    var result = new List<CardVO>();
        //    List<string> fields = GetFieldsByUser(user);
        //    if (fields == null)
        //    {
        //        return cards;
        //    }

        //    foreach (var card in cards)
        //    {
        //        bool isRejectedInAllFields = false;
        //        // FIXME: melhorar uso das instâncias por tipo de cartão
        //        switch (card.EventType)
        //        {// para melhorar o desempenho, unir os fitros de deleção e rejeição
        //            case EventType.Fire:
        //                isRejectedInAllFields = _fireEvents.IsRDORejectedInAllFields(card, fields);
        //                break;
        //            case EventType.Meteorological:
        //                isRejectedInAllFields = _meteorologicalEvents.IsRDORejectedInAllFields(card, fields);
        //                break;
        //            case EventType.TagMeasurement:
        //                isRejectedInAllFields = _tagMeasurementEvents.IsRDORejectedInAllFields(card, fields);
        //                break;
        //        }
        //        if (!isRejectedInAllFields)
        //        {
        //            result.Add(card);
        //        }
        //    }
        //    LogServiceConnection.Debug<string>($"Foram filtrados os cartões rejeitados nas áreas de atuação {fields}.");
        //    return result;
        //}

        public static List<string> GetFieldsByUser(string user)
        {
            if (RegisteredUsers.ContainsKey(user))
            {
                return RegisteredUsers[user].ToList();
            }
            return null;
        }

        public static List<string> GetRegisteredUsers()
        {
            if (RegisteredUsers != null)
            {
                return RegisteredUsers.Keys.ToList();
            }
            return new List<string>();
        }
    }
}