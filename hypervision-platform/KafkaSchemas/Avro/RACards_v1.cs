namespace KafkaSchemas.Avro
{
    using global::Avro;
    using global::Avro.Specific;
    using System;

    public partial class RACards : ISpecificRecord
    {
        public static Schema _SCHEMA = Schema.Parse(@"{""name"": ""RACards"",""namespace"": ""KafkaSchemas.Avro"",""type"": ""record"",""fields"": [{""name"": ""EVENTID"",""type"": ""string""},{""name"": ""ORIGINDATE"",""type"": {""connect.name"": ""org.apache.kafka.connect.data.Timestamp"",""connect.version"": 1,""logicalType"": ""timestamp-millis"",""type"": ""long""}},{""default"": null,""name"": ""NORMORIGINDATE"",""type"": [""null"",{""connect.name"": ""org.apache.kafka.connect.data.Timestamp"",""connect.version"": 1,""logicalType"": ""timestamp-millis"",""type"": ""long""}]},{""name"": ""UPDATEDATE"",""type"": {""connect.name"": ""org.apache.kafka.connect.data.Timestamp"",""connect.version"": 1,""logicalType"": ""timestamp-millis"",""type"": ""long""}},{""name"": ""REPOSITORYDATE"",""type"": {""connect.name"": ""org.apache.kafka.connect.data.Timestamp"",""connect.version"": 1,""logicalType"": ""timestamp-millis"",""type"": ""long""}},{""default"": null,""name"": ""NORMREPOSITORYDATE"",""type"": [""null"",{""connect.name"": ""org.apache.kafka.connect.data.Timestamp"",""connect.version"": 1,""logicalType"": ""timestamp-millis"",""type"": ""long""}]},{""name"": ""OBJID"",""type"": ""long""},{""name"": ""TAG"",""type"": ""string""},{""name"": ""AREASEGENT"",""type"": ""string""},{""default"": null,""name"": ""SYNOTIC"",""type"": [""null"",""string""]},{""name"": ""GROUP"",""type"": ""string""},{""name"": ""CLASS"",""type"": ""string""},{""name"": ""DESCRIPTION"",""type"": ""string""},{""default"": null,""name"": ""TAGREM"",""type"": [""null"",""string""]},{""name"": ""PRIORITY"",""type"": ""int""},{""default"": null,""name"": ""ALARMSTATUS"",""type"": [""null"",""string""]},{""name"": ""ALARMVALUE"",""type"": ""double""},{""default"": null,""name"": ""NORMALIZESTATUS"",""type"": [""null"",""string""]},{""default"": null,""name"": ""NORMALIZEVALUE"",""type"": [""null"",""double""]},{""default"": null,""name"": ""ORIGIN"",""type"": [""null"",""string""]},{""default"": null,""name"": ""NORMREPOSITORY"",""type"": [""null"",""string""]},{""default"": null,""name"": ""NORMORIGIN"",""type"": [""null"",""string""]},{""name"": ""GROUPHEADER"",""type"": ""boolean""},{""name"": ""NORMALIZED"",""type"": ""boolean""},{""name"": ""STATEPROTO"",""type"": ""string""},{""name"": ""ORIGINALEVENTID"",""type"": ""string""},]}");
        private string _EVENTID;
        private DateTime _ORIGINDATE;
        private System.Nullable<DateTime> _NORMORIGINDATE;
        private DateTime _UPDATEDATE;
        private DateTime _REPOSITORYDATE;
        private System.Nullable<DateTime> _NORMREPOSITORYDATE;
        private long _OBJID;
        private string _TAG;
        private string _AREASEGENT;
        private string _SYNOTIC;
        private string _GROUP;
        private string _CLASS;
        private string _DESCRIPTION;
        private string _TAGREM;
        private int _PRIORITY;
        private string _ALARMSTATUS;
        private double _ALARMVALUE;
        private string _NORMALIZESTATUS;
        private System.Nullable<double> _NORMALIZEVALUE;
        private string _ORIGIN;
        private string _NORMREPOSITORY;
        private string _NORMORIGIN;
        private bool _GROUPHEADER;
        private bool _NORMALIZED;
        private string _STATEPROTO;
        private string _ORIGINALEVENTID;

        public virtual Schema Schema
        {
            get
            {
                return RACards._SCHEMA;
            }
        }
        public string EVENTID
        {
            get
            {
                return this._EVENTID;
            }
            set
            {
                this._EVENTID = value;
            }
        }
        public DateTime ORIGINDATE
        {
            get
            {
                return this._ORIGINDATE;
            }
            set
            {
                this._ORIGINDATE = value;
            }
        }
        public System.Nullable<DateTime> NORMORIGINDATE
        {
            get
            {
                return this._NORMORIGINDATE;
            }
            set
            {
                this._NORMORIGINDATE = value;
            }
        }
        public DateTime UPDATEDATE
        {
            get
            {
                return this._UPDATEDATE;
            }
            set
            {
                this._UPDATEDATE = value;
            }
        }
        public DateTime REPOSITORYDATE
        {
            get
            {
                return this._REPOSITORYDATE;
            }
            set
            {
                this._REPOSITORYDATE = value;
            }
        }
        public System.Nullable<DateTime> NORMREPOSITORYDATE
        {
            get
            {
                return this._NORMREPOSITORYDATE;
            }
            set
            {
                this._NORMREPOSITORYDATE = value;
            }
        }
        public long OBJID
        {
            get
            {
                return this._OBJID;
            }
            set
            {
                this._OBJID = value;
            }
        }
        public string TAG
        {
            get
            {
                return this._TAG;
            }
            set
            {
                this._TAG = value;
            }
        }
        public string AREASEGENT
        {
            get
            {
                return this._AREASEGENT;
            }
            set
            {
                this._AREASEGENT = value;
            }
        }
        public string SYNOTIC
        {
            get
            {
                return this._SYNOTIC;
            }
            set
            {
                this._SYNOTIC = value;
            }
        }
        public string GROUP
        {
            get
            {
                return this._GROUP;
            }
            set
            {
                this._GROUP = value;
            }
        }
        public string CLASS
        {
            get
            {
                return this._CLASS;
            }
            set
            {
                this._CLASS = value;
            }
        }
        public string DESCRIPTION
        {
            get
            {
                return this._DESCRIPTION;
            }
            set
            {
                this._DESCRIPTION = value;
            }
        }
        public string TAGREM
        {
            get
            {
                return this._TAGREM;
            }
            set
            {
                this._TAGREM = value;
            }
        }
        public int PRIORITY
        {
            get
            {
                return this._PRIORITY;
            }
            set
            {
                this._PRIORITY = value;
            }
        }
        public string ALARMSTATUS
        {
            get
            {
                return this._ALARMSTATUS;
            }
            set
            {
                this._ALARMSTATUS = value;
            }
        }
        public double ALARMVALUE
        {
            get
            {
                return this._ALARMVALUE;
            }
            set
            {
                this._ALARMVALUE = value;
            }
        }
        public string NORMALIZESTATUS
        {
            get
            {
                return this._NORMALIZESTATUS;
            }
            set
            {
                this._NORMALIZESTATUS = value;
            }
        }
        public System.Nullable<double> NORMALIZEVALUE
        {
            get
            {
                return this._NORMALIZEVALUE;
            }
            set
            {
                this._NORMALIZEVALUE = value;
            }
        }
        public string ORIGIN
        {
            get
            {
                return this._ORIGIN;
            }
            set
            {
                this._ORIGIN = value;
            }
        }
        public string NORMREPOSITORY
        {
            get
            {
                return this._NORMREPOSITORY;
            }
            set
            {
                this._NORMREPOSITORY = value;
            }
        }
        public string NORMORIGIN
        {
            get
            {
                return this._NORMORIGIN;
            }
            set
            {
                this._NORMORIGIN = value;
            }
        }
        public bool GROUPHEADER
        {
            get
            {
                return this._GROUPHEADER;
            }
            set
            {
                this._GROUPHEADER = value;
            }
        }
        public bool NORMALIZED
        {
            get
            {
                return this._NORMALIZED;
            }
            set
            {
                this._NORMALIZED = value;
            }
        }
        public string STATEPROTO
        {
            get
            {
                return this._STATEPROTO;
            }
            set
            {
                this._STATEPROTO = value;
            }
        }

        public string ORIGINALEVENTID
        {
            get
            {
                return this._ORIGINALEVENTID;
            }
            set
            {
                this._ORIGINALEVENTID = value;
            }
        }

        public virtual object Get(int fieldPos)
        {
            switch (fieldPos)
            {
                case 0: return this.EVENTID;
                case 1: return this.ORIGINDATE;
                case 2: return this.NORMORIGINDATE;
                case 3: return this.UPDATEDATE;
                case 4: return this.REPOSITORYDATE;
                case 5: return this.NORMREPOSITORYDATE;
                case 6: return this.OBJID;
                case 7: return this.TAG;
                case 8: return this.AREASEGENT;
                case 9: return this.SYNOTIC;
                case 10: return this.GROUP;
                case 11: return this.CLASS;
                case 12: return this.DESCRIPTION;
                case 13: return this.TAGREM;
                case 14: return this.PRIORITY;
                case 15: return this.ALARMSTATUS;
                case 16: return this.ALARMVALUE;
                case 17: return this.NORMALIZESTATUS;
                case 18: return this.NORMALIZEVALUE;
                case 19: return this.ORIGIN;
                case 20: return this.NORMREPOSITORY;
                case 21: return this.NORMORIGIN;
                case 22: return this.GROUPHEADER;
                case 23: return this.NORMALIZED;
                case 24: return this.STATEPROTO;
                case 25: return this.ORIGINALEVENTID;
                default: throw new AvroRuntimeException("Bad index " + fieldPos + " in Get()");
            };
        }
        public virtual void Put(int fieldPos, object fieldValue)
        {
            switch (fieldPos)
            {
                case 0: this.EVENTID = (System.String)fieldValue; break;
                case 1: this.ORIGINDATE = (DateTime)fieldValue; break;
                case 2: this.NORMORIGINDATE = (System.Nullable<DateTime>)fieldValue; break;
                case 3: this.UPDATEDATE = (DateTime)fieldValue; break;
                case 4: this.REPOSITORYDATE = (DateTime)fieldValue; break;
                case 5: this.NORMREPOSITORYDATE = (System.Nullable<DateTime>)fieldValue; break;
                case 6: this.OBJID = (System.Int64)fieldValue; break;
                case 7: this.TAG = (System.String)fieldValue; break;
                case 8: this.AREASEGENT = (System.String)fieldValue; break;
                case 9: this.SYNOTIC = (System.String)fieldValue; break;
                case 10: this.GROUP = (System.String)fieldValue; break;
                case 11: this.CLASS = (System.String)fieldValue; break;
                case 12: this.DESCRIPTION = (System.String)fieldValue; break;
                case 13: this.TAGREM = (System.String)fieldValue; break;
                case 14: this.PRIORITY = (System.Int32)fieldValue; break;
                case 15: this.ALARMSTATUS = (System.String)fieldValue; break;
                case 16: this.ALARMVALUE = (System.Double)fieldValue; break;
                case 17: this.NORMALIZESTATUS = (System.String)fieldValue; break;
                case 18: this.NORMALIZEVALUE = (System.Nullable<double>)fieldValue; break;
                case 19: this.ORIGIN = (System.String)fieldValue; break;
                case 20: this.NORMREPOSITORY = (System.String)fieldValue; break;
                case 21: this.NORMORIGIN = (System.String)fieldValue; break;
                case 22: this.GROUPHEADER = (System.Boolean)fieldValue; break;
                case 23: this.NORMALIZED = (System.Boolean)fieldValue; break;
                case 24: this.STATEPROTO = (System.String)fieldValue; break;
                case 25: this.ORIGINALEVENTID = (System.String)fieldValue; break;
                default: throw new AvroRuntimeException("Bad index " + fieldPos + " in Put()");
            };
        }
    }
}
