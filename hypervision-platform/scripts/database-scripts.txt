use Hypervision

db.Configuration.insertOne( {
    "MinLogLevel" : "3",
    "KafkaServer" : "192.168.2.93:9092",
    "CategoryCardConfigurations" : {
        "INFO" : {
            "Category" : "0",
            "NotificationSound" : "../../../assets/audio/sirene.mp3",
            "Color" : "#359afa",
            "Name" : "Informação",
            "RepeatNotificationSound" : false
        },
        "NOTIFICATION" : {
            "Category" : "1",
            "NotificationSound" : "../../../assets/audio/sirene.mp3",
            "Color" : "#ffc107",
            "Name" : "Notificação",
            "RepeatNotificationSound" : false
        },
        "ALERT" : {
            "Category" : "2",
            "NotificationSound" : "../../../assets/audio/sirene.mp3",
            "Color" : "#00ff32",
            "Name" : "Alerta",
            "RepeatNotificationSound" : false
        },        
        "ALARM" : {
            "Category" : "3",
            "NotificationSound" : "../../../assets/audio/alarme.mp3",
            "Color" : "#c0392b",
            "Name" : "Alarme",
            "RepeatNotificationSound" : false
        },        
    },
    "TimelineConfigurations" : {
        "CurrentTimeColor" : "#0c1469",
        "GraphBackgroundColor" : "#273238",
        "ResumeBackgroundColor" : "#aaaaaa",
        "FontColor" : "#65707a",
        "FontSize" : 13,
        "FontFamily" : "Roboto",
        "Heigth" : 150,
        "InitialLimit": 1,
        "FinalLimit:" 3
    },
    "EventFeedConfigurations" : {
        "FeedBackgroundColor" : "#111619",
        "CardBackgroundColor" : "#090a0c",
        "DashboardBackgroundColor" : "#111619",
        "FontColor" : "#ffffff",
        "FeedWidth" : "29%",
        "Levels" : [],
        "DisplayDateTime" : true,
        "DisplayFilter" : true,
        "AllowMultiselectionCard" : true,
        "SoundAlert" : false,
        "AllowHideCard" : true
    }
});