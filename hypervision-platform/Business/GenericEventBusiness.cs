﻿using hypervision_platform.DAO;
using hypervision_platform.DVO;
using hypervision_platform.Models;
using hypervision_platform.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace hypervision_platform
{
    public class GenericEventBusiness
    {
        private readonly RdoDAO _rdoDAO;

        public GenericEventBusiness(RdoDAO rdoDAO)
        {
            _rdoDAO = rdoDAO;
        }

        internal List<CardVO> GetRDOById(string id)
        {
            var rdos = _rdoDAO.GetRDOById(id);

            return rdos.Select(x => new CardVO
            {
                Id = x.CardId.ToString(),
                Level = x.Level,
                Severity = x.Severity,
                Date = x.Date,
                EventStart = x.EventStart,
                EventEnd = x.EventEnd,
                EventType = x.EventType,
                CardDetails = x.CardDetails,
                Dispached = x.Dispached,
                LastEvent = x.Event,
                MergedCardId = x.MergedCardId
            }).ToList();
        }

        internal bool IsRDODeletedByUser(CardVO card, string user)
        {
            if (ActionType.Delete.Equals(card.LastEvent.Type) && user.Equals(card.LastEvent.User))
            {
                return true;
            }
            return false;
        }

        internal bool IsRDORejectedInAllFields(CardVO card, List<string> fields)
        {

            if (ActionType.Reject.Equals(card.LastEvent.Type))
            {
                var notRejectedAreas = card.CardDetails.Regions.Except(card.LastEvent.Regions);
                return notRejectedAreas.Intersect(fields).Count() == 0;
            }
            return false;
        }

        internal bool IsRDOForwarded(string id)
        {
            List<Card> card = _rdoDAO.GetRDOById(id);
            foreach (Card cardAction in card)
            {
                if (ActionType.Forward.Equals(cardAction.Event.Type))
                {
                    return true;
                }
            }
            return false;
        }

        public CardVO AddAction(ActionType type, string cardId, string user, string comment = null, List<string> fields = null, string receiverUser = null)
        {
            var rdos = _rdoDAO.GetRDOByIdOrThrowsException(cardId);
            var c = rdos.Last();

            c.Event = new CardEvent
            {
                Date = DateTime.Now.ToUniversalTime(),
                Description = CardEvent.GetActionDescription(type),
                Comment = comment,
                Type = type,
                User = user,
                Regions = fields,
                ReceiverUser = receiverUser
            };

            var cards = _rdoDAO.UpdateCard(c).OrderBy(c => c.Event.Date);
            var lastrdo = Converters.CardEntityToVO(cards.Last());
            var eventsPerUser = cards.GroupBy(c => c.Event.User, g => new { g });
            lastrdo.EventHistory = _rdoDAO.GetLastEventsByUser(cardId);
            if (rdos.Any(_ => _.Event.Type == ActionType.Finish))
            {
                lastrdo.LastEvent = rdos.FirstOrDefault(_ => _.Event.Type == ActionType.Finish).Event;
            }
            return lastrdo;
        }

        public Card MergeRDOs(string user, CardVO mainCardRequest, List<CardVO> cardsRequest, string comment)
        {
            Card mainCard = Converters.CardVOToEntity(mainCardRequest);
            var cards = cardsRequest.Select(c => Converters.CardVOToEntity(c)).ToList();
            return new MergeCard().MergeRDOs(user, mainCard, cards, comment);
        }


    }
}
