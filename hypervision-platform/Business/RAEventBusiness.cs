using KafkaSchemas.Avro;
using Commons_Core.Services.LogService;
using hypervision_platform.DAO;
using hypervision_platform.Models;
using hypervision_platform.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;

namespace hypervision_platform
{
    public class RAEventBusiness : BaseEventBusiness<RACards>
    {
        private readonly RdoDAO _rdoDAO;
        public RAEventBusiness(RdoDAO rdoDAO) : base(rdoDAO)
        {
            _rdoDAO = rdoDAO;
        }
        internal override Card SaveOrUpdateRDOFromTopic(RACards data)
        {
            try
            {
                var card = new Card
                {
                    CardId = data.EVENTID,
                    Date = data.REPOSITORYDATE,
                    Dispached = false,
                    Severity = Severity.ALERT,
                    Level = data.TAG,
                    EventStart = data.REPOSITORYDATE,
                    //EventEnd = data.NORMREPOSITORYDATE,// TODO: verificar se deve ser encerrado ao normalizar
                    EventType = EventType.RA,
                    CardDetails = new CardDetails()
                    {
                        Tag = data.TAG,
                        Status = data.STATEPROTO, 
                        Description = data.DESCRIPTION
                    }
                };
                if (data.TAGREM != null) {
                    card.CardDetails.Substations = new List<string> { data.TAGREM };
                }
                var areaseg = ConfigurationBusiness.ConvertArea(data.AREASEGENT);
                if (areaseg != null) {
                    card.CardDetails.Regions = new List<string> { areaseg };
                }

                var rdoForEvent = _rdoDAO.FilterBy(_ => _.CardId == card.CardId).ToList();

                if (rdoForEvent == null || rdoForEvent.Count == 0)
                {
                    card.Event = new CardEvent
                    {
                        Date = DateTime.Now.ToUniversalTime(),
                        Description = "Evento recebido",
                        Type = ActionType.None
                    };
                    card.CardDetails.Properties = new List<List<Property>> { GetAlarmsProperties(data) };

                    LogServiceConnection.Debug<string>("Evento de RA recebido.");
                }
                else
                {
                    card.Event = new CardEvent
                    {
                        Date = DateTime.Now.ToUniversalTime(),
                        Description = "Evento atualizado",
                        Type = rdoForEvent.Last().Event.Type,
                        User = rdoForEvent.Last().Event.User
                    };

                    if (rdoForEvent.Last().CardDetails.Properties != null)
                    {
                        card.CardDetails.Properties = rdoForEvent.Last().CardDetails.Properties;
                        // verificar se existe eventos de atualização de um mesmo alarme
                        var relatedEventIndex = card.CardDetails.Properties.FindIndex(p => p[p.Count - 1].Value == data.ORIGINALEVENTID);
                        if(relatedEventIndex >= 0)
                        {
                            card.CardDetails.Properties[relatedEventIndex] = GetAlarmsProperties(data);
                        }
                        else
                        {
                            card.CardDetails.Properties.Add(GetAlarmsProperties(data));
                        }
                    } 
                    else
                    {
                        card.CardDetails.Properties = new List<List<Property>> { GetAlarmsProperties(data) };
                    }

                    LogServiceConnection.Debug<string>("Evento de RA atualizado."); 
                }

                _rdoDAO.InsertOne(card);

                LogServiceConnection.Debug<string>("Evento de RA persistido.");
                return card;
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao tentar salvar um rdo de RA: {ex.Message}");
                return null;
            }
        }

        private List<Property> GetAlarmsProperties(RACards data)
        {
            var properties = new List<Property>();

            properties.Add(new Property { Label = "Data início", Value = data.ORIGINDATE.ToString() });
            properties.Add(new Property { Label = "Estado", Value = data.STATEPROTO });
            properties.Add(new Property { Label = "Estado alarme", Value = data.ALARMSTATUS });
            properties.Add(new Property { Label = "Data normalização", Value = data.NORMORIGINDATE?.ToString() });
            properties.Add(new Property { Label = "Estado normalização", Value = data.NORMALIZESTATUS });
            properties.Add(new Property { Label = "Tag", Value = data.TAG });
            properties.Add(new Property { Label = "Área", Value = ConfigurationBusiness.ConvertArea(data.AREASEGENT) });
            properties.Add(new Property { Label = "Substação", Value = data.TAGREM });
            properties.Add(new Property { Label = "Descrição", Value = data.DESCRIPTION });
            properties.Add(new Property { Label = "Valor alarme", Value = data.ALARMVALUE.ToString() });
            properties.Add(new Property { Label = "Valor normalização", Value = data.NORMALIZEVALUE?.ToString() });
            properties.Add(new Property { Label = "Prioridade", Value = data.PRIORITY.ToString() });
            properties.Add(new Property { Label = "Sinótico", Value = data.SYNOTIC });
            properties.Add(new Property { Label = "Grupo", Value = data.GROUP });
            properties.Add(new Property { Label = "Classe", Value = data.CLASS });
            properties.Add(new Property { Label = "Id objeto", Value = data.OBJID.ToString() });
            properties.Add(new Property { Label = "Data atualização", Value = data.UPDATEDATE.ToString() });
            // Manter sempre no último da lista. É um dado que não deve ser exibido, só utilizado pra filtro
            properties.Add(new Property { Label = "Id original", Value = data.ORIGINALEVENTID });

            return properties;
        }

        internal override void SetConfigurations()
        {
            this.TopicName = Environment.GetEnvironmentVariable("RA_CARD_TOPIC_NAME");
            this.RDO_Name = "RACard";
        }

    }
}