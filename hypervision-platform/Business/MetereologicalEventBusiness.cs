using Commons_Core.Services.LogService;
using hypervision_platform.DAO;
using hypervision_platform.Models;
using hypervision_platform.Models.Enum;
using KafkaSchemas.Avro;
using System;
using System.Linq;

namespace hypervision_platform
{
    public class MetereologicalEventBusiness : BaseEventBusiness<MeteorologicalCards>
    {
        private readonly RdoDAO _rdoDAO;

        public MetereologicalEventBusiness(RdoDAO rdoDAO) : base(rdoDAO)
        {
            _rdoDAO = rdoDAO;
        }
        internal override Card SaveOrUpdateRDOFromTopic(MeteorologicalCards data)
        {
            try
            {
                var card = new Card
                {
                    CardId = data.Id,
                    Date = DateTime.Parse(data.Date).ToUniversalTime(),
                    Dispached = false,
                    Severity = Severity.NOTIFICATION,
                    Level = data.Level,
                    EventStart = DateTime.Parse(data.StartDate).ToUniversalTime(),
                    EventEnd = DateTime.Parse(data.EndDate).ToUniversalTime(),
                    EventType = EventType.Meteorological,
                    CardDetails = new CardDetails
                    {
                        Regions = data.Regions.Split(',').ToList(),
                        Cities = data.Cities.Split(',').ToList(),
                        Substations = data.Substations.Split(',').ToList(),
                        Lines = data.Lines.Split(',').ToList()
                    }
                };
                var rdoForEvent = _rdoDAO.FilterBy(_ => _.CardId == card.CardId).ToList();

                if (rdoForEvent == null || rdoForEvent.Count == 0)
                {
                    card.Event = new CardEvent
                    {
                        Date = DateTime.Now.ToUniversalTime(),
                        Description = "Evento recebido",
                        Type = ActionType.None
                    };

                    LogServiceConnection.Debug<string>("Evento de meteorologico recebido.");
                }
                else
                {
                    card.Event = new CardEvent
                    {
                        Date = DateTime.Now.ToUniversalTime(),
                        Description = "Evento atualizado",
                        Type = rdoForEvent.Last().Event.Type,
                        User = rdoForEvent.Last().Event.User
                    };

                    LogServiceConnection.Debug<string>("Evento de meteorologico atualizado.");
                }
                _rdoDAO.InsertOne(card);
                LogServiceConnection.Debug<string>("Evento de meteorologico persistido.");

                return card;
            }
            catch (Exception ex)
            {
                LogServiceConnection.Error<string>($"Erro ao tentar salvar um rdo: {ex.Message}");
                return null;
            }
        }

        internal override void SetConfigurations()
        {
            this.TopicName = Environment.GetEnvironmentVariable("METEOROLOGICAL_CARD_TOPIC_NAME");
            this.RDO_Name = "MeterologicalCard";
        }
    }
}
