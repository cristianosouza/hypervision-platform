﻿using Commons_Core.Services.LogService;
using hypervision_platform.DAO;
using hypervision_platform.Models;
using System;

namespace hypervision_platform
{
    public class EventBusinessFactory
    {
        public static GenericEventBusiness CreateBusinessByType(EventType eventType, RdoDAO rdoDAO)
        {
            switch (eventType)
            {
                case EventType.Meteorological:
                    return new MetereologicalEventBusiness(rdoDAO);
                case EventType.Fire:
                    return new FireEventBusiness(rdoDAO);
                case EventType.TagMeasurement:
                    return new TagMeasurementEventBusiness(rdoDAO);
                case EventType.RA:
                    return new RAEventBusiness(rdoDAO);
                    case EventType.Fault:
                    return new FaultEventBusiness(rdoDAO);
                default:
                    string msg = $"Tipo de evento desconhecido: {eventType}. Não foi possível criar instância do business.";
                    LogServiceConnection.Error<string>(msg);
                    throw new Exception(msg);
            }
        }
    }
}
