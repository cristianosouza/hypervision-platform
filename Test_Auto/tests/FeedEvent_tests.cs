using OpenQA.Selenium;
using Test_Auto.pages;
using Test_Auto.utils;
using NUnit.Framework;

namespace Test_Auto.tests
{
    public class FeedEvent_tests : WebDriverConfig
    {
        //variables
        IWebDriver driver;
        FeedEvent_POM feedEventsTest;
        Login_POM loginTest;

        // string checkBoxFilterAlarm = "Filtrar Alarme";
        // string checkBoxFilterAlert = "Filtrar Alerta";
        // string checkBoxFilterNotif = "Filtrar Notificação";
        // string checkBoxFilterInform = "Filtrar Informação";

        string startDate = "31-12-1999";
        string timeForTest = "00:01";
        string endDate = "01-01-2000";
        string textForFilter = "ALERTA QUEIMADAS";

        [SetUp]
        public void OpenChrome()
        {
            driver = WebDriverInitialConfig();
            driver.Navigate().GoToUrl("http://192.168.2.94:5001/login");
            loginTest = new Login_POM(driver);
            loginTest.DoLogin();
        }

        [Test]
        public void T1_FeedEventItemsOnScreen()//precisa refatorar!!!!!!!!
        {
            feedEventsTest = new FeedEvent_POM(driver);
            Assert.True(feedEventsTest.InputStartDate.Displayed);
            Assert.True(feedEventsTest.EndDateFilter.Displayed);
            Assert.True(feedEventsTest.DescriptionFilter.Displayed);

            Assert.True(feedEventsTest.BtnDisplayHideCards.Displayed);
            Assert.True(feedEventsTest.BtnHideSelectedCard.Displayed);
            Assert.True(feedEventsTest.BtnSeverity.Displayed);







        }

        [Test]
        public void T2_TestAllFilters()// Aguardando cartões .....
        {
            feedEventsTest = new FeedEvent_POM(driver);
            Assert.True(feedEventsTest.InputStartDate.Displayed);
            Assert.True(feedEventsTest.InputStartDate.Displayed);
        }

        [TearDown]
        public void CloseChrome()
        {
            WebDriverClose(driver);
        }
    }
}