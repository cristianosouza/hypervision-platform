using OpenQA.Selenium;
using Test_Auto.pages;
using Test_Auto.utils;
using NUnit.Framework;
using System;
using System.Threading;

namespace Test_Auto.tests
{
    public class NavBar_tests : WebDriverConfig
    {
        //variables
        IWebDriver driver;
        NavBar_POM navBarTest;
        Login_POM loginTest;
        SettingPlatform_POM settingCOD;
        FeedEvent_POM feedEventsTest;

        [SetUp]
        public void OpenChrome()
        {
            driver = WebDriverInitialConfig();
            driver.Navigate().GoToUrl("http://192.168.2.94:5001/login");
            loginTest = new Login_POM(driver);
            loginTest.DoLogin();
        }

        [Test]
        public void T1_ItemsOnScreenNavBar()//***************ok!!!!
        {
            navBarTest = new NavBar_POM(driver);
            Assert.True(navBarTest.ImgCemig.Displayed);
            Assert.True(navBarTest.TitleNavBar.Text == "Plataforma Hypervision");
            Assert.True(navBarTest.DataProfile.Displayed);
            Assert.True(navBarTest.ButtonListApps.Displayed);
        }

        [Test]
        public void T2_BtnLogout() //***************ok!!!!
        {
            navBarTest = new NavBar_POM(driver);
            navBarTest.DataProfile.Click();
            navBarTest.BtnLogout.Click();
            loginTest = new Login_POM(driver);
            Assert.True(loginTest.OnLoginScreen());
        }

        [Test]
        public void T3_ListApps()//***************ok!!!!
        {
            navBarTest = new NavBar_POM(driver);
            navBarTest.ButtonListApps.Click();
            Assert.True(navBarTest.ButtonListApps.Displayed);
            //Console.WriteLine(navBarTest.UserManagement.Text);
            Assert.True(navBarTest.UserManagement.Text == "Gestão de Usuários");
            Assert.True(navBarTest.Meteorology.Text == "Meteorologia");
            Assert.True(navBarTest.FireAlert.Text == "Queimadas");
            Assert.True(navBarTest.Settings.Text == "Configurações");
        }

        [Test]
        public void T4_AppUserManagement()//***************ok!!!!
        {
            navBarTest = new NavBar_POM(driver);
            navBarTest.ButtonListApps.Click();

            //Store the ID of the original window
            string originalWindow = driver.CurrentWindowHandle;
            //Check we don't have other windows open already
            Assert.AreEqual(driver.WindowHandles.Count, 1);
            //Click the link which opens in a new window
            navBarTest.UserManagement.Click();
            //Loop through until we find a new window handle
            foreach (string window in driver.WindowHandles)
            {
                if (originalWindow != window)
                {
                    driver.SwitchTo().Window(window);
                    break;
                }
            }
            IWebElement titulo = driver.FindElement(By.CssSelector(".title-container > .text-center"));
            Assert.AreEqual(titulo.Text, "Gestão de usuários");
        }

        [Test]
        public void T5_AppWeatherAlert()
        {
            /*Este APP ainda não foi implementado */
            Console.WriteLine("\n***************************************");
            Console.WriteLine("Aguardando implementação - Meteorologia");
            Console.WriteLine("***************************************");
        }

        [Test]
        public void T6_AppFireAlert()
        {
            /* Este APP ainda não foi implementado*/
            Console.WriteLine("\n***************************************");
            Console.WriteLine("Aguardando implementação - Queimadas");
            Console.WriteLine("***************************************");
        }

        [Test]
        public void T7_AppSettings()
        {
            navBarTest = new NavBar_POM(driver);
            navBarTest.ButtonListApps.Click();
            navBarTest.Settings.Click();
            settingCOD = new SettingPlatform_POM(driver);
            Thread.Sleep(1000);
            settingCOD.BtnBack.Click();
            feedEventsTest = new FeedEvent_POM(driver);
            Assert.True(feedEventsTest.InputStartDate.Displayed);
        }

        [TearDown]
        public void CloseChrome()
        {
            WebDriverClose(driver);
        }
    }
}