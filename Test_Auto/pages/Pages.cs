using OpenQA.Selenium;
using System.Collections.Generic;
using System;

namespace Test_Auto.pages
{
    public class Pages
    {
        public IWebDriver driver;
        public Pages(IWebDriver driver)
        {
            this.driver = driver;
        }

        public List<IWebElement> CreateListElements(IWebElement fatherElement, string targetElement)
        {    //this method will create a list of elements
            List<IWebElement> listOfElements = new List<IWebElement>();
            foreach (IWebElement el in fatherElement.FindElements(By.TagName(targetElement)))
            {
                listOfElements.Add(el);
            }

            return listOfElements;
        }

        //CreateListElements
        public IWebElement ChooseOneInList(string myChoice, List<IWebElement> elementList)
        {//this method will return the chosen WebElement
            foreach (IWebElement e in elementList)
            {
                if (e.Text == myChoice)
                {
                    Console.WriteLine(e.Text);
                    return e;
                }
            }
            return null;
        }
        public void ChooseInSelect(string myChoice, List<IWebElement> listSelect)
        {
            foreach (IWebElement e in listSelect)
            {
                if (e.Text == myChoice)
                {
                    e.Click();
                    // System.Console.WriteLine(e.Text);
                }
            }
        }
    }
}