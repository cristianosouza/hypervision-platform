using OpenQA.Selenium;
namespace Test_Auto.pages
{
    public class NavBar_POM : Pages
    {
        Login_POM loginTest;
        NavBar_POM navBarTest;
        bool passed = false;
        public NavBar_POM(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
        }
        public IWebElement ImgCemig { get => driver.FindElement(By.CssSelector("#navbarHeaderBackMenu > img")); }
        public IWebElement TitleNavBar { get => driver.FindElement(By.CssSelector("#navbarHeaderBackMenu > h4")); }

        //_________________________________________________________________________________________________
        public IWebElement ButtonListApps { get => driver.FindElement(By.CssSelector(".fa-th")); }
        public IWebElement UserManagement { get => driver.FindElement(By.CssSelector("div.col-sm-auto:nth-child(1) > a:nth-child(1) > div:nth-child(1) > span:nth-child(2)")); }
        public IWebElement Meteorology { get => driver.FindElement(By.CssSelector("div.col-sm-auto:nth-child(2) > a:nth-child(1) > div:nth-child(1) > span:nth-child(2)")); }
        public IWebElement FireAlert { get => driver.FindElement(By.CssSelector("div.col-sm-auto:nth-child(3) > a:nth-child(1) > div:nth-child(1) > span:nth-child(2)")); }
        public IWebElement Settings { get => driver.FindElement(By.CssSelector("div.col-sm-auto:nth-child(4) > a:nth-child(1) > div:nth-child(1) > span:nth-child(2)")); }
        //***************************************************************************************************

        public IWebElement DataProfile { get => driver.FindElement(By.Id("userIcon")); }
        public IWebElement InfoUser { get => driver.FindElement(By.CssSelector("div.dropdown-item")); }
        public IWebElement BtnLogout { get => driver.FindElement(By.CssSelector("#navbarMenuList > li.dropdown.show > div > a:nth-child(4)")); }

        public bool DoLoginForTest()
        {
            loginTest = new Login_POM(driver);
            loginTest.DoLogin();
            navBarTest = new NavBar_POM(driver);
            passed = (navBarTest.TitleNavBar.Text == "Plataforma Hypervision");
            return (passed);

        }



    }
}