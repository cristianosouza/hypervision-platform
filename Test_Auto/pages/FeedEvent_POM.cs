using OpenQA.Selenium;
using System.Collections.Generic;

namespace Test_Auto.pages
{

    public class FeedEvent_POM : Pages
    {
        public FeedEvent_POM(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
        }

        //Date init, and Date End, and Filter Description
        public IWebElement InputStartDate { get => driver.FindElement(By.Id("inputStartDate")); }
        public IWebElement EndDateFilter { get => driver.FindElement(By.Id("inputEndDate")); }
        public IWebElement DescriptionFilter { get => driver.FindElement(By.Id("inputDescription")); }

        //Buttons  - Hide Card and Show Cards (which were hidden)
        public IWebElement BtnDisplayHideCards { get => driver.FindElement(By.Id("btnDisplay")); }
        public IWebElement BtnHideSelectedCard { get => driver.FindElement(By.Id("btnHide")); }
        public IWebElement BtnSeverity { get => driver.FindElement(By.Id("btnSeverity")); }

        //all cards will be here
        public IWebElement FeedCardsBody { get => driver.FindElement(By.Id("feedBody")); }
        public IWebElement AllLevelsFilter { get => driver.FindElement(By.Id(".levelsFilter")); }



        

        //list buttons in card
        public IWebElement AllButtonsInCard { get => driver.FindElement(By.Id("cardActions")); }
        
        public void SetStartDatetime(string date, string time)
        {
            InputStartDate.SendKeys(date);
            InputStartDate.SendKeys(Keys.Tab);
            InputStartDate.SendKeys(time);
        }
        public void SetEndtDatetime(string date, string time)
        {
            EndDateFilter.SendKeys(date);
            EndDateFilter.SendKeys(Keys.Tab);
            EndDateFilter.SendKeys(time);
        }



    }
}