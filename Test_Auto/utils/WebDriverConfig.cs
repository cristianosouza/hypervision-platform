using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using OpenQA.Selenium.Remote;

namespace Test_Auto.utils
{
    public class WebDriverConfig
    {
        public IWebDriver WebDriverInitialConfig()
        {
            var options = new ChromeOptions();
            options.AddArgument("no-sandbox");
            options.AddArgument("--headless");
            options.AddArgument("--verbose");
            IWebDriver driver = new ChromeDriver(options);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);

            return driver;
        }
        public void WebDriverClose(IWebDriver driver)
        {
            try
            {
                driver.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}